package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Cmt2cmt {
    @SerializedName("messages")
    private List<Messages> mMessages;

    public List<Messages> getMessages() {
        return mMessages;
    }

    public void setMessages(List<Messages> mMessages) {
        this.mMessages = mMessages;
    }
}