//package jp.co.primesecurity.tapdoorcloud.mvp.view.adapters;
//
//import android.content.Context;
//import android.content.DialogInterface;
//import android.graphics.Color;
//import android.support.v4.content.ContextCompat;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.widget.RecyclerView;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import jp.co.primesecurity.tapdoorcloud.mvp.view.activities.MainActivity;
//import jp.co.primesecurity.tapdoorcloud.util.DialogUtils;
//import jp.co.primesecurity.tapdoorcloud.util.RxBus;
//
//import java.util.List;
//
//import butterknife.BindView;
//import io.reactivex.disposables.CompositeDisposable;
//import jp.co.primesecurity.tapdoorcloud.R;
//import jp.co.primesecurity.tapdoorcloud.mvp.base.baseadapter.BaseRecyclerAdapter;
//import jp.co.primesecurity.tapdoorcloud.mvp.base.baseviewholder.BaseItemViewHolder;
//import jp.co.primesecurity.tapdoorcloud.mvp.model.network.event.ClickEditTagEvent;
//import jp.co.primesecurity.tapdoorcloud.mvp.model.network.event.EditTagEvent;
//import jp.co.primesecurity.tapdoorcloud.mvp.model.data.Tags;
//
//
//public class TagAdapter extends BaseRecyclerAdapter<Tags, TagAdapter.TagHolder> {
//
//    private List<String> mTagsCheck;
//    private CompositeDisposable mCompositeDisposable;
//    private RxBus mRxBus;
//
//    public TagAdapter(Context context, List<Tags> tags, List<String> tagsCheck) {
//        super(context, tags);
//        mTagsCheck = tagsCheck;
//        mCompositeDisposable = new CompositeDisposable();
//        if (context instanceof MainActivity) {
//            mRxBus = ((MainActivity) context).getRxBusSingleton();
//        }
//    }
//
//    @Override
//    protected int getItemResourceLayout(int viewType) {
//        return R.layout.row_tag;
//    }
//
//    @Override
//    public TagHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = getView(parent, viewType);
//        return new TagHolder(view, mItemClickListener, mTagsCheck, mCompositeDisposable, mRxBus);
//    }
//
//    public static class TagHolder extends BaseItemViewHolder<Tags> {
//        @BindView(R.id.view)
//        View mView;
//        @BindView(R.id.tv_tag_name)
//        TextView mTvTagName;
//        @BindView(R.id.iv_tag_check)
//        ImageView mIvTagCheck;
//        @BindView(R.id.iv_edit)
//        ImageView mIvEdit;
//
//        private List<String> mTagsCheck;
//        private CompositeDisposable mCompositeDisposable;
//        private RxBus mRxBus;
//
//        public TagHolder(View itemView, OnItemClickListener onItemClickListener, List<String> tagsCheck, CompositeDisposable compositeDisposable, RxBus rxBus) {
//            super(itemView, onItemClickListener, null);
//            mTagsCheck = tagsCheck;
//            mCompositeDisposable = compositeDisposable;
//            mRxBus = rxBus;
//        }
//
//        @Override
//        public void bind(Tags tags, int position) {
//            mView.setBackgroundColor(Color.parseColor(tags.getColor()));
//            mTvTagName.setText(tags.getTitle());
//            mIvTagCheck.setVisibility(getTags(tags.getId()) ? View.VISIBLE : View.GONE);
//            mIvEdit.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (mRxBus.hasObservers()) {
//                        mRxBus.send(new ClickEditTagEvent());
//                    }
//                    Context context = view.getContext();
//                    AlertDialog alertDialog = DialogUtils.getAlertDialogCustom(context, R.layout.create_new_tag, R.string.save, R.string.clear, new DialogUtils.AppDialogCallBack() {
//                        @Override
//                        public void onClickDialog(DialogInterface pAlertDialog, int pDialogType) {
//                            if (pDialogType == DialogInterface.BUTTON_POSITIVE) {
//                                if (mRxBus.hasObservers()) {
//                                    mRxBus.send(new EditTagEvent(true, tags));
//                                }
//                            } else if (pDialogType == DialogInterface.BUTTON_NEGATIVE) {
//                                DialogUtils.getOkCancelAlertDialog(context, R.string.confirm_remove_tag, new DialogUtils.AppDialogCallBack() {
//                                    @Override
//                                    public void onClickDialog(DialogInterface pAlertDialog, int pDialogType) {
//                                        if (pDialogType == DialogInterface.BUTTON_POSITIVE) {
//                                            if (mRxBus.hasObservers()) {
//                                                mRxBus.send(new EditTagEvent(false, tags));
//                                            }
//                                        }
//                                    }
//                                }).show();
//                            }
//                        }
//                    });
//                    alertDialog.show();
//                    EditText editText = alertDialog.findViewById(R.id.editText);
//                    Button btnPositive = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
//                    btnPositive.setEnabled(false);
//                    btnPositive.setTextColor(ContextCompat.getColor(context, R.color.selector_save_create_tag));
//                    if (editText != null) {
//                        editText.setText(tags.getTitle());
//                        editText.setSelection(tags.getTitle().length());
//                        editText.addTextChangedListener(new TextWatcher() {
//                            @Override
//                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                            }
//
//                            @Override
//                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//                            }
//
//                            @Override
//                            public void afterTextChanged(Editable editable) {
//                                String string = editable.toString();
//                                if (string.length() > 0) {
//                                    btnPositive.setEnabled(true);
//                                } else {
//                                    btnPositive.setEnabled(false);
//                                }
//                                tags.setTitle(string);
//                            }
//                        });
//                    }
//                }
//            });
//        }
//
//        @Override
//        public void onClick(View v) {
//            super.onClick(v);
//            mIvTagCheck.setVisibility((mIvTagCheck.getVisibility() == View.VISIBLE) ? View.GONE : View.VISIBLE);
//        }
//
//        private boolean getTags(String tags) {
//            return mTagsCheck.contains(tags);
//        }
//    }
//
//    @Override
//    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
//        super.onDetachedFromRecyclerView(recyclerView);
//        mCompositeDisposable.clear();
//    }
//}
