package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Customers implements Parcelable {
    @SerializedName("fbname")
    private String mFbname;
    @SerializedName("fbid")
    private String mFbid;
    @SerializedName("_id")
    private String mId;

    public String getFbname() {
        return mFbname;
    }

    public void setFbname(String mFbname) {
        this.mFbname = mFbname;
    }

    public String getFbid() {
        return mFbid;
    }

    public void setFbid(String mFbid) {
        this.mFbid = mFbid;
    }

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mFbname);
        dest.writeString(this.mFbid);
        dest.writeString(this.mId);
    }

    public Customers() {
    }

    protected Customers(Parcel in) {
        this.mFbname = in.readString();
        this.mFbid = in.readString();
        this.mId = in.readString();
    }

    public static final Creator<Customers> CREATOR = new Creator<Customers>() {
        @Override
        public Customers createFromParcel(Parcel source) {
            return new Customers(source);
        }

        @Override
        public Customers[] newArray(int size) {
            return new Customers[size];
        }
    };
}