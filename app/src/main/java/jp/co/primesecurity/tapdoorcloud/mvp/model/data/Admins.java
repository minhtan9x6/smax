package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Admins {
    @SerializedName("sid")
    private int mSid;
    @SerializedName("fbid")
    private String mFbid;
    @SerializedName("fbname")
    private String mFbname;
    @SerializedName("fbtoken")
    private String mFbtoken;
    @SerializedName("update")
    private String mUpdate;
    @SerializedName("_id")
    private String mId;
    @SerializedName("error")
    private boolean mError;
    @SerializedName("fbperms")
    private List<String> mFbPerms;

    public int getSid() {
        return mSid;
    }

    public void setSid(int mSid) {
        this.mSid = mSid;
    }

    public String getFbid() {
        return mFbid;
    }

    public void setFbid(String mFbid) {
        this.mFbid = mFbid;
    }

    public String getFbname() {
        return mFbname;
    }

    public void setFbname(String mFbname) {
        this.mFbname = mFbname;
    }

    public String getFbtoken() {
        return mFbtoken;
    }

    public void setFbtoken(String mFbtoken) {
        this.mFbtoken = mFbtoken;
    }

    public String getUpdate() {
        return mUpdate;
    }

    public void setUpdate(String mUpdate) {
        this.mUpdate = mUpdate;
    }

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public boolean getError() {
        return mError;
    }

    public void setError(boolean mError) {
        this.mError = mError;
    }

    public List<String> getFbPerms() {
        return mFbPerms;
    }

    public void setFbPerms(List<String> mFbperms) {
        this.mFbPerms = mFbperms;
    }
}