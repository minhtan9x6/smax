//package jp.co.primesecurity.tapdoorcloud.mvp.view.adapters;
//
//import android.content.Context;
//
//
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.TextView;
//
//import java.util.List;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import jp.co.primesecurity.tapdoorcloud.R;
//import jp.co.primesecurity.tapdoorcloud.mvp.base.baseadapter.BaseRecyclerAdapter;
//import jp.co.primesecurity.tapdoorcloud.mvp.base.baseviewholder.BaseItemViewHolder;
//import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.Repository;
//
//
//public class RepositoryAdapter extends BaseRecyclerAdapter<Repository, RepositoryAdapter.RepositoryViewHolder> {
//
//
//    public RepositoryAdapter(Context context, List<Repository> repositories) {
//        super(context, repositories);
//    }
//
//    @Override
//    protected int getItemResourceLayout(int viewType) {
//        return R.layout.item_repo;
//    }
//
//    @Override
//    public RepositoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = getView(parent, viewType);
//        return new RepositoryViewHolder(view);
//    }
//
//    public static class RepositoryViewHolder extends BaseItemViewHolder<Repository> {
//        @BindView(R.id.layout_content)
//        public View contentLayout;
//        @BindView(R.id.text_repo_title)
//        public TextView titleTextView;
//        @BindView(R.id.text_repo_description)
//        public TextView descriptionTextView;
//        @BindView(R.id.text_watchers)
//        public TextView watchersTextView;
//        @BindView(R.id.text_stars)
//        public TextView starsTextView;
//        @BindView(R.id.text_forks)
//        public TextView forksTextView;
//        public Repository repository;
//
//        public RepositoryViewHolder(View itemView) {
//            super(itemView);
//            ButterKnife.bind(this, itemView);
//        }
//
//        @Override
//        public void bind(Repository repository, int position) {
//            Context context = contentLayout.getContext();
//            titleTextView.setText(repository.name);
//            descriptionTextView.setText(repository.description);
//            watchersTextView.setText(
//                    context.getResources().getString(R.string.text_watchers, repository.watchers));
//            starsTextView.setText(
//                    context.getResources().getString(R.string.text_stars, repository.stars));
//            forksTextView.setText(
//                    context.getResources().getString(R.string.text_forks, repository.forks));
//        }
//    }
//}
