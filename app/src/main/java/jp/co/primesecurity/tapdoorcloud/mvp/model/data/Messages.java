package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.blankj.utilcode.util.StringUtils;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class Messages {
    @SerializedName("_id")
    private String mId;
    @SerializedName("fbcreated_time")
    private String mFbcreatedTime;
    @SerializedName("type")
    private String mType;
    @SerializedName("thid")
    private String mThid;
    @SerializedName("fbpageid")
    private String mFbPageId;
    @SerializedName("fbmsg")
    private String mFbmsg;
    @SerializedName("fbparent_id")
    private String mFbparentId;
    @SerializedName("fbid")
    private String mFbId;
    @SerializedName("__v")
    private int mV;
    @SerializedName("viewer")
    private Viewer mViewer;
    @SerializedName("fbsender")
    private FbSender mFbSender;
    @SerializedName("fbshares")
    private List<FbShare> mFbShares = null;
    @SerializedName("user_likes")
    private boolean mIsUserLikes;
    @SerializedName("is_hidden")
    private boolean mIsHidden;
    @SerializedName("attach")
    private Attach mAttach;
    private FbAttachment mFbAttachment;
    private List<FbAttachmentInbox> mFbAttachmentInboxes;
    @SerializedName("deleter")
    private Deleter mDeleter;
    @SerializedName("privatereplied")
    private boolean mIsPrivateReplied;
    @SerializedName("can_comment")
    private boolean mCanComment;
    @SerializedName("can_hide")
    private boolean mCanHide;
    @SerializedName("can_like")
    private boolean mCanLike;
    @SerializedName("can_remove")
    private boolean mCanRemove;
    @SerializedName("can_reply_privately")
    private boolean mCanReplyPrivately;


    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getFbcreatedTime() {
        return mFbcreatedTime;
    }

    public void setFbcreatedTime(String mFbcreatedTime) {
        this.mFbcreatedTime = mFbcreatedTime;
    }

    public String getType() {
        return mType;
    }

    public void setType(String mType) {
        this.mType = mType;
    }

    public String getThid() {
        return mThid;
    }

    public void setThid(String mThid) {
        this.mThid = mThid;
    }

    public String getFbPageId() {
        return mFbPageId;
    }

    public void setFbPageId(String mFbpageid) {
        this.mFbPageId = mFbpageid;
    }

    /*public FbAttachment getFbAttachment() {
        return mFbAttachment;
    }

    public void setFbAttachment(FbAttachment mFbattachment) {
        this.mFbAttachment = mFbattachment;
    }

    public List<FbAttachmentInbox> getFbAttachmentInbox() {
        return mFbAttachmentInbox;
    }

    public void setFbAttachmentInbox(List<FbAttachmentInbox> fbAttachmentInbox) {
        mFbAttachmentInbox = fbAttachmentInbox;
    }*/

    public String getFbmsg() {
        return mFbmsg;
    }

    public void setFbmsg(String mFbmsg) {
        this.mFbmsg = mFbmsg;
    }

    public String getFbparentId() {
        return mFbparentId;
    }

    public void setFbparentId(String mFbparentId) {
        this.mFbparentId = mFbparentId;
    }

    public String getFbId() {
        return mFbId;
    }

    public void setFbId(String mFbid) {
        this.mFbId = mFbid;
    }

    public int getV() {
        return mV;
    }

    public void setV(int mV) {
        this.mV = mV;
    }

    public Viewer getViewer() {
        return mViewer;
    }

    public void setViewer(Viewer mViewer) {
        this.mViewer = mViewer;
    }

    public FbSender getFbSender() {
        return mFbSender;
    }

    public void setFbSender(FbSender mFbsender) {
        this.mFbSender = mFbsender;
    }

    public List<FbShare> getFbShares() {
        return mFbShares;
    }

    public void setFbShares(List<FbShare> mFbshares) {
        this.mFbShares = mFbshares;
    }

    public Attach getAttach() {
        return mAttach;
    }

    public void setAttach(Attach attach) {
        mAttach = attach;
    }

    public boolean isUserLikes() {
        return mIsUserLikes;
    }

    public void setUserLikes(boolean userLikes) {
        mIsUserLikes = userLikes;
    }

    public boolean isHidden() {
        return mIsHidden;
    }

    public void setHidden(boolean hidden) {
        mIsHidden = hidden;
    }

    public Deleter getDeleter() {
        return mDeleter;
    }

    public void setDeleter(Deleter deleter) {
        mDeleter = deleter;
    }

    public boolean isPrivateReplied() {
        return mIsPrivateReplied;
    }

    public void setPrivateReplied(boolean privateReplied) {
        mIsPrivateReplied = privateReplied;
    }

    public boolean getCanComment() {
        return mCanComment;
    }

    public void setCanComment(boolean mCanComment) {
        this.mCanComment = mCanComment;
    }

    public boolean getCanHide() {
        return mCanHide;
    }

    public void setCanHide(boolean mCanHide) {
        this.mCanHide = mCanHide;
    }

    public boolean getCanLike() {
        return mCanLike;
    }

    public void setCanLike(boolean mCanLike) {
        this.mCanLike = mCanLike;
    }

    public boolean getCanRemove() {
        return mCanRemove;
    }

    public void setCanRemove(boolean mCanRemove) {
        this.mCanRemove = mCanRemove;
    }

    public boolean getCanReplyPrivately() {
        return mCanReplyPrivately;
    }

    public void setCanReplyPrivately(boolean mCanReplyPrivately) {
        this.mCanReplyPrivately = mCanReplyPrivately;
    }

    public FbAttachment getFbAttachment() {
        return mFbAttachment;
    }

    public void setFbAttachment(FbAttachment fbAttachment) {
        mFbAttachment = fbAttachment;
    }

    public List<FbAttachmentInbox> getFbAttachmentInboxes() {
        return mFbAttachmentInboxes;
    }

    public void setFbAttachmentInboxes(List<FbAttachmentInbox> fbAttachmentInboxes) {
        mFbAttachmentInboxes = fbAttachmentInboxes;
    }

    public static class OptionsDeserilizer implements JsonDeserializer<Object> {

        @Override
        public Messages deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Messages messages = new Gson().fromJson(json, Messages.class);
            JsonObject jsonObject = json.getAsJsonObject();

            if (jsonObject.has("fbattachment")) {
                JsonElement elem = jsonObject.get("fbattachment");
                if (elem != null && !elem.isJsonNull()) {
                    if (elem.isJsonArray()) {
                        String valueArray = elem.getAsJsonArray().toString();
                        if (!StringUtils.isEmpty(valueArray)) {
                            Type listType = new TypeToken<List<FbAttachmentInbox>>(){}.getType();
                            List<FbAttachmentInbox> fbAttachment = new Gson().fromJson(valueArray, listType);
                            messages.setFbAttachmentInboxes(fbAttachment);
                        }
                    } else if (elem.isJsonObject()) {
                        String valueObject = elem.getAsJsonObject().toString();
                        if (!StringUtils.isEmpty(valueObject)) {
                            FbAttachment fbAttachment = new Gson().fromJson(valueObject, FbAttachment.class);
                            messages.setFbAttachment(fbAttachment);
                        }
                    }
                }
            }
            return messages;
        }
    }
}