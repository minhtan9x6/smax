package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

public class ModeRange {
    @SerializedName("since")
    private int mSince;
    @SerializedName("until")
    private int mUntil;
    @SerializedName("status")
    private int mStatus;
    @SerializedName("begin")
    private String begin;
    @SerializedName("end")
    private String end;

    public int getSince() {
        return mSince;
    }

    public void setSince(int mSince) {
        this.mSince = mSince;
    }

    public int getUntil() {
        return mUntil;
    }

    public void setUntil(int mUntil) {
        this.mUntil = mUntil;
    }

    public int getStatus() {
        return mStatus;
    }

    public void setStatus(int mStatus) {
        this.mStatus = mStatus;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
}