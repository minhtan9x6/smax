package jp.co.primesecurity.tapdoorcloud.mvp.view.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.co.primesecurity.tapdoorcloud.R;
import jp.co.primesecurity.tapdoorcloud.mvp.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends BaseFragment {

    @Override
    protected int getContentResId() {
        return R.layout.fragment_login;
    }

    @Override
    protected void init() {

    }

    @Override
    public Context getMyContext() {
        return getContext();
    }
}
