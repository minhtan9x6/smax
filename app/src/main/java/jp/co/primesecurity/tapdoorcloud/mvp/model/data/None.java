package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

public class None {
    @SerializedName("ib2ib")
    private Ib2ib mIb2ib;
    @SerializedName("cmt2ib")
    private Cmt2ib mCmt2ib;
    @SerializedName("cmt2cmt")
    private Cmt2cmt mCmt2cmt;

    public Ib2ib getIb2ib() {
        return mIb2ib;
    }

    public void setIb2ib(Ib2ib mIb2ib) {
        this.mIb2ib = mIb2ib;
    }

    public Cmt2ib getCmt2ib() {
        return mCmt2ib;
    }

    public void setCmt2ib(Cmt2ib mCmt2ib) {
        this.mCmt2ib = mCmt2ib;
    }

    public Cmt2cmt getCmt2cmt() {
        return mCmt2cmt;
    }

    public void setCmt2cmt(Cmt2cmt mCmt2cmt) {
        this.mCmt2cmt = mCmt2cmt;
    }
}