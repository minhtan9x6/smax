package jp.co.primesecurity.tapdoorcloud.mvp.model.network.tasks;

import android.content.Context;

import jp.co.primesecurity.tapdoorcloud.mvp.model.network.request.ThreadRequest;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.DetailFbPageResponse;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.InboxResponse;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.PopulationRankResponse;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.ApiServiceInterface;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.AppService;

import io.reactivex.Observable;

/**
 * Created by thanhvn on 12/23/17.
 */

public class GetBiPageInboxTask extends BaseBiCallbackTask<DetailFbPageResponse, InboxResponse> {

    private String mFbId;
    private ThreadRequest mThreadRequest;

    public GetBiPageInboxTask(Context context, boolean isLoading, String fbId, ThreadRequest threadRequest) {
        super(context, isLoading);
        mFbId = fbId;
        mThreadRequest = threadRequest;
    }

    @Override
    public Observable<DetailFbPageResponse> processAction1() {
        return AppService.getInstance().createServiceWithAccessToken(getContext()).getDetailFbPage(mFbId);
    }

    @Override
    public Observable<InboxResponse> processAction2() {
        return AppService.getInstance().createServiceWithAccessToken(getContext()).postInboxThread(mFbId, mThreadRequest);
    }
}
