package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

public class Deleter {
    @SerializedName("sid")
    private int mSid;
    @SerializedName("name")
    private String mName;
    @SerializedName("time")
    private String mTime;
    @SerializedName("fbid")
    private String mFbid;
    @SerializedName("fbname")
    private String mFbname;

    public int getSid() {
        return mSid;
    }

    public void setSid(int mSid) {
        this.mSid = mSid;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getTime() {
        return mTime;
    }

    public void setTime(String mTime) {
        this.mTime = mTime;
    }

    public String getFbid() {
        return mFbid;
    }

    public void setFbid(String mFbid) {
        this.mFbid = mFbid;
    }

    public String getFbname() {
        return mFbname;
    }

    public void setFbname(String mFbname) {
        this.mFbname = mFbname;
    }
}