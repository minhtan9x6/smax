package jp.co.primesecurity.tapdoorcloud.mvp.model.network.tasks;

import android.content.Context;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by thanh.vn on 6/5/2017.
 */

public abstract class BaseCallbackTask<T> extends BaseCallback<T> {

    public BaseCallbackTask(Context context, boolean showLoading) {
        super(context, showLoading);
    }

    public BaseCallbackTask(Context context) {
        this(context, false);
    }

    protected abstract Observable<T> processAction();

    public DisposableObserver observable(OnFinishCallbackListener onFinishCallbackListener) {
        mOnFinishCallbackListener = onFinishCallbackListener;
        return processSubscribe(processAction()).subscribeWith(new DisposableObserver<T>() {
            @Override
            public void onNext(T value) {
                if (value != null) {
                    onSuccessful(value);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }
}