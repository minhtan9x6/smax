package jp.co.primesecurity.tapdoorcloud.mvp.model.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by thanh.vn on 6/9/2017.
 */

public class CountriesResponse {

    @SerializedName("countries")
    private List<String> countries = null;

    public List<String> getCountries() {
        return countries;
    }

    public void setCountries(List<String> countries) {
        this.countries = countries;
    }
}
