package jp.co.primesecurity.tapdoorcloud.mvp.base.baseviewholder;

import android.content.Context;
import android.view.View;

import butterknife.ButterKnife;

public abstract class BaseListViewHolder<Data> {
    protected Context mContext;

    public BaseListViewHolder(View itemView) {
        ButterKnife.bind(this, itemView);
    }

    public abstract void bind(Data data);
}