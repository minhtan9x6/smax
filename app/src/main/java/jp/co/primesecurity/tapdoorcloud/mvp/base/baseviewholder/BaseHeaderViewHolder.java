package jp.co.primesecurity.tapdoorcloud.mvp.base.baseviewholder;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import butterknife.ButterKnife;

public abstract class BaseHeaderViewHolder extends BaseItemViewHolder {
    protected Context mContext;
    protected Bundle mBundle;

    public BaseHeaderViewHolder(View itemView) {
        super(itemView, null, null);
        ButterKnife.bind(this, itemView);
    }

    public BaseHeaderViewHolder(View itemView, Bundle bundle) {
        super(itemView, null, null);
        ButterKnife.bind(this, itemView);
        this.mBundle = bundle;
    }

    @Override
    public void bind(Object o, int position) {

    }

    public abstract void show();

    public void saveState(Bundle bundle) {
        this.mBundle = bundle;
    }
}