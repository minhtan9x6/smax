package jp.co.primesecurity.tapdoorcloud.mvp.base;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ScreenUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;


public abstract class BaseActivity extends AppCompatActivity implements MvpView {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private final List<KeyBackPressCallBack> mKeyBackPressList = new ArrayList<>();
    private int mContentMainId;
    protected ActionBar mActionBar;
    protected Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentViewResId());
        ButterKnife.bind(this);
        ScreenUtils.setPortrait(this);
        init();
    }

    protected abstract int getContentViewResId();
    protected abstract void init();

    protected void addFragment(Fragment targetFragment, String tag, int layoutResId) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.addToBackStack(targetFragment.getClass().getName());
        transaction.replace(layoutResId, targetFragment, tag)
                .setTransitionStyle(FragmentTransaction.TRANSIT_NONE)
                .commit();
    }

    protected void addFragment(Fragment targetFragment, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.addToBackStack(targetFragment.getClass().getName());
        transaction.replace(getContentMainId(), targetFragment, tag)
                .setTransitionStyle(FragmentTransaction.TRANSIT_NONE)
                .commit();
    }


    /**
     * Call back when user pressed Back Button Device
     */
    public interface KeyBackPressCallBack {
        void handleBackPress();
    }

    public void addKeyBackCallBack(KeyBackPressCallBack pKeyBackPressCallBack) {
        this.mKeyBackPressList.add(pKeyBackPressCallBack);
    }

    public void removeKeyBackCallBack(KeyBackPressCallBack pKeyBackPressCallBack) {
        this.mKeyBackPressList.remove(pKeyBackPressCallBack);
    }

    public Fragment getFragmentById(int resId) {
        return getSupportFragmentManager().findFragmentById(resId);
    }

    public Fragment getCurrentFragment() {
        int index = this.getSupportFragmentManager().getBackStackEntryCount() - 1;
        FragmentManager.BackStackEntry backEntry = this.getSupportFragmentManager().getBackStackEntryAt(index);
        String tag = backEntry.getName();
        return this.getSupportFragmentManager().findFragmentByTag(tag);
    }

    protected void setupToolbar(final Toolbar toolbar) {
        setupToolbar(toolbar, null);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected void setupToolbar(final Toolbar toolbar, final View.OnClickListener onClickListener) {

        mToolbar = toolbar;
        setSupportActionBar(toolbar);
        mActionBar = getSupportActionBar();
        if (mActionBar != null)
            mActionBar.setHomeButtonEnabled(true);

        if (onClickListener != null)
            toolbar.setNavigationOnClickListener(onClickListener);
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override
    public void setTitle(int title) {
        super.setTitle(title);
        if (mActionBar != null)
            mActionBar.setTitle(getString(title));
    }

    public ActionBar getBaseActionBar() {
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        return actionBar;
    }

    @Override
    public void onBackPressed() {
        // Disable default behavior of Back Button.
        // super.onBackPressed();
        if (!mKeyBackPressList.isEmpty()) {
            for (KeyBackPressCallBack pKeyBackCallBack : mKeyBackPressList) {
                if (pKeyBackCallBack != null) {
                    pKeyBackCallBack.handleBackPress();
                }
            }
        } else {
            super.onBackPressed();
            LogUtils.e("onBackPressed", "Call back is empty");
        }
    }

    public int getContentMainId() {
        return mContentMainId;
    }

    public void setContentMainId(int contentMainId) {
        mContentMainId = contentMainId;
    }

    public FragmentManager.OnBackStackChangedListener getListener() {
        return () -> {
            List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
            if (fragmentList != null && !fragmentList.isEmpty()) {
                for (Fragment fragment : fragmentList) {
                    if (fragment != null) {
                        fragment.onResume();
                    }
                }
            }
        };
    }
}
