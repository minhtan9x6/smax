package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Admin {
    @SerializedName("_id")
    private String mId;
    @SerializedName("fbfan_count")
    private int mFbfanCount;
    @SerializedName("fbpicture")
    private String mFbpicture;
    @SerializedName("fbname")
    private String mFbname;
    @SerializedName("fbid")
    private String mFbid;
    @SerializedName("statistics")
    private Statistics mStatistics;
    @SerializedName("active")
    private Active mActive;
    @SerializedName("shares")
    private List<Shares> mShares;
    @SerializedName("admins")
    private List<Admins> mAdmins;

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public int getFbfanCount() {
        return mFbfanCount;
    }

    public void setFbfanCount(int mFbfanCount) {
        this.mFbfanCount = mFbfanCount;
    }

    public String getFbpicture() {
        return mFbpicture;
    }

    public void setFbpicture(String mFbpicture) {
        this.mFbpicture = mFbpicture;
    }

    public String getFbname() {
        return mFbname;
    }

    public void setFbname(String mFbname) {
        this.mFbname = mFbname;
    }

    public String getFbid() {
        return mFbid;
    }

    public void setFbid(String mFbid) {
        this.mFbid = mFbid;
    }

    public Statistics getStatistics() {
        return mStatistics;
    }

    public void setStatistics(Statistics mStatistics) {
        this.mStatistics = mStatistics;
    }

    public Active getActive() {
        return mActive;
    }

    public void setActive(Active mActive) {
        this.mActive = mActive;
    }

    public List<Shares> getShares() {
        return mShares;
    }

    public void setShares(List<Shares> mShares) {
        this.mShares = mShares;
    }

    public List<Admins> getAdmins() {
        return mAdmins;
    }

    public void setAdmins(List<Admins> mAdmins) {
        this.mAdmins = mAdmins;
    }
}