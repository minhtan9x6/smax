package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by thanhvn on 1/3/18.
 */

public class Attach {
    @SerializedName("attach_type")
    private String mAttachType;
    @SerializedName("url")
    private String mUrl;

    public String getAttachType() {
        return mAttachType;
    }

    public void setAttachType(String mAttachType) {
        this.mAttachType = mAttachType;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }
}
