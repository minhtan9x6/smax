package jp.co.primesecurity.tapdoorcloud.mvp.model.network.tasks;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;


import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import java.io.IOException;
import java.lang.annotation.Annotation;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.ErrorResponse;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.ApiConfig;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.AppService;
import okhttp3.Headers;
import okhttp3.ResponseBody;
import retrofit2.Converter;

/**
 * Created by thanh.vn on 6/21/2017.
 */

public abstract class BaseCallback<T> {
    private String TAG = this.getClass().toString();
    private boolean mIsShowLoading = true;
    private android.app.Dialog mDialog;
    private Context mContext;
    private static final int SUCCESS = 200;
    private static final int ERROR_INPUT = 400;
    private static final int UNAUTHORIZED = 401;
    private static final int FORBIDDEN = 403;
    private static final int NOT_FOUND = 404;
    private static final int REQUEST_TIMEOUT = 408;
    private static final int INTERNAL_SERVER_ERROR = 500;
    private static final int BAD_GATEWAY = 502;
    private static final int SERVICE_UNAVAILABLE = 503;
    private static final int GATEWAY_TIMEOUT = 504;

    /*
    Const for exception
     */
    private int CONST_EXCEPTION = 0;
    private int CONST_TIME_OUT = 1;
    private int CONST_NOT_FOUND = 2;
    private int CONST_AUTHENTICATE = 3;
    private int CONST_SERVER_ERROR = 4;

    private int mStateObservable;


    public BaseCallback(Context context, boolean showLoading) {
        this.mContext = context;
        this.mIsShowLoading = showLoading;
        if (showLoading) {
            if (getContext() instanceof Activity) {
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mDialog = new ProgressDialog(mContext);
                        mDialog.setCancelable(false);
                    }
                });
            }
        }
    }

    public BaseCallback(Context context) {
        this(context, false);
    }

    public Context getContext() {
        return mContext;
    }

    // Call back when task processed
    protected OnFinishCallbackListener<T> mOnFinishCallbackListener;

    protected Observable<T> processSubscribe(Observable<T> observable) {
        if (!NetworkUtils.isConnected()) {
            onNoInternetAccess();
            dismissDiaLogLoading();
            return Observable.empty();
        } else {
            return observable
                    .doOnSubscribe(disposable -> {
                        mStateObservable = 0;
                        showDiaLogLoading(mIsShowLoading);
                    })
                    .doOnDispose(() -> mStateObservable = 1)
                    .doOnComplete(() -> {
                        mStateObservable = 2;
                    })
                    .doOnError(throwable -> {
                        mStateObservable = 3;
                    })
                    .doOnTerminate(() -> {
                        mStateObservable = 4;
                        dismissDiaLogLoading();
                    })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .onErrorResumeNext(this::handleOnError);
        }
    }

    private Observable<T> handleOnError(Throwable throwable) {
        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            int status = httpException.code();
            if (status == ERROR_INPUT) {
                ResponseBody responseBody = httpException.response().errorBody();
                if (responseBody != null) {
                    Converter<ResponseBody, ErrorResponse> errorConverter =
                            AppService.getInstance().restClient(mContext, ApiConfig.getApiUrl(), false).responseBodyConverter(ErrorResponse.class, new Annotation[0]);
                    try {
                        ErrorResponse error = errorConverter.convert(responseBody);
                        if (error != null) {
                            // show errorMsg here
//                            mContext.runOnUiThread(() -> DialogUtils.getBackAlertDialog(mContext, error.getDetail(), null).show());
                            onError(error, null);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //DO ERROR HANDLING HERE
                }
            } else {
                getErrorFromCode(status);
            }
        }
        return Observable.empty();
    }

    public void onSuccessful(T pSuccess, Headers headers) {
        LogUtils.d("AppConnectBaseTask", "onSuccess");
        if (mOnFinishCallbackListener != null) {
            mOnFinishCallbackListener.onSuccess(pSuccess, headers);
        }
    }

    public void onSuccessful(T pSuccess) {
        LogUtils.d("AppConnectBaseTask", "onSuccess");
        if (mOnFinishCallbackListener != null) {
            mOnFinishCallbackListener.onSuccess(pSuccess, null);
        }
    }

    public void onError(ErrorResponse error, Headers headers) {
        LogUtils.d("AppConnectBaseTask", "onError");
        if (mOnFinishCallbackListener != null) {
            mOnFinishCallbackListener.onError(error, headers);
        }
    }

    public void onError(ErrorResponse error) {
        LogUtils.d("AppConnectBaseTask", "onError");
        if (mOnFinishCallbackListener != null) {
            mOnFinishCallbackListener.onError(error, null);
        }
    }

    public void onFailed(Integer pReturnFailed) {
        LogUtils.e(TAG, "onFailed");
        if (mOnFinishCallbackListener != null) {
            mOnFinishCallbackListener.onFailed(pReturnFailed);
        }
    }

    private void getErrorFromCode(int pReturnFailed) {
        switch (pReturnFailed) {
            case NOT_FOUND:
                onDocumentNotFound();
                break;
            case UNAUTHORIZED:
                onAuthenticatedFail();
                break;
            case REQUEST_TIMEOUT:
            case GATEWAY_TIMEOUT:
                onConnectionTimeout();
                break;
            case INTERNAL_SERVER_ERROR:
            case BAD_GATEWAY:
            case SERVICE_UNAVAILABLE:
                onServerError(pReturnFailed);
                break;
            default:
                onException(pReturnFailed);
                break;
        }
    }

    public void onException() {
        LogUtils.d("AppConnectBaseTask", "onException");
        onFailed(CONST_EXCEPTION);
    }

    public void onException(int status) {
        LogUtils.d("AppConnectBaseTask", "onException");
        onFailed(CONST_EXCEPTION);
    }

    public void onConnectionTimeout() {
        LogUtils.d("AppConnectBaseTask", "onConnectionTimeout");
        onFailed(CONST_TIME_OUT);
    }

    public void onDocumentNotFound() {
        LogUtils.d("AppConnectBaseTask", "onDocumentNotFound");
        onFailed(CONST_NOT_FOUND);
    }

    public void onAuthenticatedFail() {
        LogUtils.d("AppConnectBaseTask", "onAuthenticatedFail");
        onFailed(CONST_AUTHENTICATE);
    }

    public void onNoInternetAccess() {
        LogUtils.d("AppConnectBaseTask", "onNoInternetAccess");
        onFailed(CONST_EXCEPTION);
    }

    public void onServerError() {
        LogUtils.d("AppConnectBaseTask", "onServerError");
        onFailed(CONST_SERVER_ERROR);
    }

    public void onServerError(int status) {
        LogUtils.d("AppConnectBaseTask", "onServerError");
        onFailed(CONST_SERVER_ERROR);
    }

    public interface OnFinishCallbackListener<T> {
        void onSuccess(T pResult, Headers headers);

        void onError(Object error, Headers headers);

        void onFailed(Integer pResult);
    }

    private void dismissDiaLogLoading() {
        if (mIsShowLoading || (mDialog != null && mDialog.isShowing())) {
            try {
                mDialog.dismiss();
            } catch (Throwable ignored) {
            }
            mDialog = null;
        }
    }

    private void showDiaLogLoading(boolean pIsShow) {
        if (mIsShowLoading || mDialog != null) {
            if (mDialog == null) {
                mDialog = new ProgressDialog(mContext);
                mDialog.setCancelable(false);
            }
            if (pIsShow) {
                try {
                    mDialog.show();
                } catch (Throwable ignored) {
                }
            }
        }
    }
}
