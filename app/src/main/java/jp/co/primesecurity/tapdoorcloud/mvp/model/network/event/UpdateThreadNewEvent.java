package jp.co.primesecurity.tapdoorcloud.mvp.model.network.event;

import jp.co.primesecurity.tapdoorcloud.mvp.model.data.Threads;

/**
 * Created by thanhvn on 1/12/18.
 */

public class UpdateThreadNewEvent {
    private Threads mThreads;

    public UpdateThreadNewEvent(Threads threads) {
        mThreads = threads;
    }

    public Threads getThreads() {
        return mThreads;
    }

    public void setThreads(Threads threads) {
        mThreads = threads;
    }
}
