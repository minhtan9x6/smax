package jp.co.primesecurity.tapdoorcloud.mvp.model.network;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import jp.co.primesecurity.tapdoorcloud.BuildConfig;
import jp.co.primesecurity.tapdoorcloud.common.AppConst;
import jp.co.primesecurity.tapdoorcloud.mvp.model.prefs.AppSharedPreferences;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.platform.Platform;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class AppService {

    private Activity mActivity;

    private static final int DEFAULT_TIMEOUT = 5;

    private static AppService sAppService;

    private String accessToken = "";

    public static AppService getInstance() {
        if (sAppService == null) {
            sAppService = new AppService();
        }
        return sAppService;
    }

    private OkHttpClient configClient(Context context, final boolean addAccessToken) {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        SharedPreferences mPref = context.getSharedPreferences(AppConst.PREFERENCE_NAME, Context.MODE_PRIVATE);
        AppSharedPreferences mAppSharedPreferences = AppSharedPreferences.getInstance(mPref);
        accessToken = mAppSharedPreferences.getAccessToken();

        Interceptor headerIntercept = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request.Builder builder = chain.request().newBuilder();
                builder.header("Accept", "application/json")
                        .header("User-Agent", AppConst.OK_HTTP_USER_AGENT);

                /*if (!TextUtils.isEmpty(accessToken) && addAccessToken) {
                    builder.addHeader("Authorization", "Bearer " + accessToken);
                } else {
                    builder.addHeader("Authorization", "Basic ");
                }*/
                Request request = builder.build();
                return chain.proceed(request);
            }
        };

        // Log
        if (BuildConfig.DEBUG) {
            okHttpClient.addInterceptor(new LoggingInterceptor.Builder()
                    .loggable(BuildConfig.DEBUG)
                    .setLevel(Level.BODY)
                    .log(Platform.INFO)
                    .request("Request")
                    .response("Response")
                    .addHeader("version", BuildConfig.VERSION_NAME)
                    .build());
        }

        okHttpClient.connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        okHttpClient.addNetworkInterceptor(headerIntercept);
        okHttpClient.connectTimeout(10, TimeUnit.SECONDS);
        okHttpClient.readTimeout(20, TimeUnit.SECONDS);
        okHttpClient.writeTimeout(20, TimeUnit.SECONDS);
        okHttpClient.retryOnConnectionFailure(true);

        return okHttpClient.build();
    }

    public Retrofit restClient(Context context, final String urlHost, final boolean addAccessToken) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        return new Retrofit.Builder()
                .baseUrl(urlHost)
                .client(configClient(context, addAccessToken))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    private <T> T createService(Class<T> serviceClass, Context context, final String urlHost, final boolean addAccessToken) {
        Retrofit retrofit = restClient(context, urlHost, addAccessToken);
        return retrofit.create(serviceClass);
    }

    public ApiServiceInterface createServiceWithAccessToken(Context context) {
        return createService(ApiServiceInterface.class, context, ApiConfig.getApiUrl(), true);
    }

    public ApiServiceInterface createService(Context context) {
        return createService(ApiServiceInterface.class, context, ApiConfig.getApiUrl(), false);
    }
}
