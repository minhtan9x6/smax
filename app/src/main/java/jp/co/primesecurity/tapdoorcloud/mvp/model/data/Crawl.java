package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

public class Crawl {
    @SerializedName("photo")
    private Photo mPhoto;
    @SerializedName("inbox")
    private Inbox mInbox;
    @SerializedName("post")
    private Post mPost;

    public Photo getPhoto() {
        return mPhoto;
    }

    public void setPhoto(Photo mPhoto) {
        this.mPhoto = mPhoto;
    }

    public Inbox getInbox() {
        return mInbox;
    }

    public void setInbox(Inbox mInbox) {
        this.mInbox = mInbox;
    }

    public Post getPost() {
        return mPost;
    }

    public void setPost(Post mPost) {
        this.mPost = mPost;
    }
}