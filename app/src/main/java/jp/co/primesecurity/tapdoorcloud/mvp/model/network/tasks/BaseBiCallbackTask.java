package jp.co.primesecurity.tapdoorcloud.mvp.model.network.tasks;

import android.content.Context;

import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

/**
 * Created by thanh.vn on 6/21/2017.
 */

public abstract class BaseBiCallbackTask<T1, T2> extends BaseCallback {

    private List<Object> mCombineResult;
    private OnCombineCallbackListener mOnCombineCallbackListener;

    public BaseBiCallbackTask(Context context, boolean showLoading) {
        super(context, showLoading);
    }

    public BaseBiCallbackTask(Context context) {
        super(context, false);
    }

    public abstract Observable<T1> processAction1();

    public abstract Observable<T2> processAction2();

    public interface OnCombineCallbackListener {
        void combineResult(Object result1, Object result2);
    }

    public Disposable setOnCombineCallbackListener(OnCombineCallbackListener onCombineCallbackListener) {
        mOnCombineCallbackListener = onCombineCallbackListener;
        return Flowable.zip(flowable(processAction1()), flowable(processAction2()), (o1, o2) -> {
            mOnCombineCallbackListener.combineResult(o1, o2);
            return "";
        }).subscribe();
    }

    private Flowable flowable(Observable observable) {
        return processSubscribe(observable).toFlowable(BackpressureStrategy.BUFFER);
    }
}
