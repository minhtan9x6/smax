package jp.co.primesecurity.tapdoorcloud.mvp.view.mvpview;

import jp.co.primesecurity.tapdoorcloud.mvp.base.MvpView;
import jp.co.primesecurity.tapdoorcloud.mvp.model.data.Tags;

import java.util.List;

import jp.co.primesecurity.tapdoorcloud.mvp.model.data.Threads;




public interface PageInboxMvpView extends MvpView {

    void showInfoPage(List<Threads> threads, List<Tags> tags);
    void showThreads(List<Threads> threads);
    void updateThreadNew(Threads threads);
}