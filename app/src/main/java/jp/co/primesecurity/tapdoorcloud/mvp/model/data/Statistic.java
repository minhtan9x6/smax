package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

public class Statistic {
    @SerializedName("update")
    private String mUpdate;
    @SerializedName("fbpage_use")
    private int mFbpageUse;
    @SerializedName("fbpage")
    private int mFbpage;
    @SerializedName("fbpage_active")
    private int mFbpageActive;
    @SerializedName("status")
    private int mStatus;

    public String getUpdate() {
        return mUpdate;
    }

    public void setUpdate(String mUpdate) {
        this.mUpdate = mUpdate;
    }

    public int getFbpageUse() {
        return mFbpageUse;
    }

    public void setFbpageUse(int mFbpageUse) {
        this.mFbpageUse = mFbpageUse;
    }

    public int getFbpage() {
        return mFbpage;
    }

    public void setFbpage(int mFbpage) {
        this.mFbpage = mFbpage;
    }

    public int getFbpageActive() {
        return mFbpageActive;
    }

    public void setFbpageActive(int mFbpageActive) {
        this.mFbpageActive = mFbpageActive;
    }

    public int getStatus() {
        return mStatus;
    }

    public void setStatus(int mStatus) {
        this.mStatus = mStatus;
    }
}
