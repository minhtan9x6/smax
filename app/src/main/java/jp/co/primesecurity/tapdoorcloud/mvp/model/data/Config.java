package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class  Config implements Parcelable {
    @SerializedName("auto_hide_phone")
    private boolean mAutoHidePhone;
    @SerializedName("show_tag")
    private int mShowTag;
    @SerializedName("phones")
    private List<String> mPhones;
    @SerializedName("auto_hide")
    private boolean mAutoHide;
    @SerializedName("isnew_otheapp")
    private boolean mIsnewOtheapp;
    @SerializedName("reply_quickly")
    private boolean mReplyQuickly;
    @SerializedName("shop")
    private Shop mShop;

    public Config(boolean autoHidePhone, boolean autoHide, boolean isnewOtheapp, boolean replyQuickly) {
        mAutoHidePhone = autoHidePhone;
        mAutoHide = autoHide;
        mIsnewOtheapp = isnewOtheapp;
        mReplyQuickly = replyQuickly;
    }

    public boolean getAutoHidePhone() {
        return mAutoHidePhone;
    }

    public void setAutoHidePhone(boolean mAutoHidePhone) {
        this.mAutoHidePhone = mAutoHidePhone;
    }

    public int getShowTag() {
        return mShowTag;
    }

    public void setShowTag(int mShowTag) {
        this.mShowTag = mShowTag;
    }

    public List<String> getPhones() {
        return mPhones;
    }

    public void setPhones(List<String> mPhones) {
        this.mPhones = mPhones;
    }

    public boolean getAutoHide() {
        return mAutoHide;
    }

    public void setAutoHide(boolean mAutoHide) {
        this.mAutoHide = mAutoHide;
    }

    public boolean getIsnewOtheapp() {
        return mIsnewOtheapp;
    }

    public void setIsnewOtheapp(boolean mIsnewOtheapp) {
        this.mIsnewOtheapp = mIsnewOtheapp;
    }

    public boolean getReplyQuickly() {
        return mReplyQuickly;
    }

    public void setReplyQuickly(boolean mReplyQuickly) {
        this.mReplyQuickly = mReplyQuickly;
    }

    public Shop getShop() {
        return mShop;
    }

    public void setShop(Shop mShop) {
        this.mShop = mShop;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.mAutoHidePhone ? (byte) 1 : (byte) 0);
        dest.writeInt(this.mShowTag);
        dest.writeStringList(this.mPhones);
        dest.writeByte(this.mAutoHide ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mIsnewOtheapp ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mReplyQuickly ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.mShop, flags);
    }

    protected Config(Parcel in) {
        this.mAutoHidePhone = in.readByte() != 0;
        this.mShowTag = in.readInt();
        this.mPhones = in.createStringArrayList();
        this.mAutoHide = in.readByte() != 0;
        this.mIsnewOtheapp = in.readByte() != 0;
        this.mReplyQuickly = in.readByte() != 0;
        this.mShop = in.readParcelable(Shop.class.getClassLoader());
    }

    public static final Creator<Config> CREATOR = new Creator<Config>() {
        @Override
        public Config createFromParcel(Parcel source) {
            return new Config(source);
        }

        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };
}