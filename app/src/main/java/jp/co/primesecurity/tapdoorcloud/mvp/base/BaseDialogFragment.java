package jp.co.primesecurity.tapdoorcloud.mvp.base;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by thanhvn on 10/9/17.
 */

public abstract class BaseDialogFragment extends DialogFragment implements MvpView {

    protected Context mContext;
    protected LayoutInflater mInflater;

    protected abstract int getContentResId();
    protected abstract void init(Dialog dialog);

    protected BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mContext = getBaseActivity();
        mInflater = LayoutInflater.from(mContext);
        View view = mInflater.inflate(getContentResId(), null);
        ButterKnife.bind(this, view);
        return setupDialog(view);
    }

    protected Dialog setupDialog(View view) {
        Dialog dialog = new Dialog(mContext);
        dialog.setContentView(view);
        init(dialog);
        return dialog;
    }
}
