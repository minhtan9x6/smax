package jp.co.primesecurity.tapdoorcloud.util.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by 4qualia on 2/7/2017.
 * Copyright © 2016年 tcl. All rights reserved.
 */

public class MeasureARelativeLayout extends RelativeLayout {
    public MeasureARelativeLayout(Context context) {
        super(context);
    }

    public MeasureARelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MeasureARelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MeasureARelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Set a square layout.
        super.onMeasure(widthMeasureSpec, widthMeasureSpec * 3/4 );
    }
}
