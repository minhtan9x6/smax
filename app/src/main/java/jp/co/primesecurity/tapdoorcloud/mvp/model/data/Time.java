package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

public class Time {
    @SerializedName("format")
    private Format format;
    @SerializedName("mode_range")
    private ModeRange modeRange;
    @SerializedName("begin")
    private int mBegin;
    @SerializedName("end")
    private int mEnd;

    public Time(int begin, int end) {
        mBegin = begin;
        mEnd = end;
    }

    public Format getFormat() {
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }

    public ModeRange getModeRange() {
        return modeRange;
    }

    public void setModeRange(ModeRange modeRange) {
        this.modeRange = modeRange;
    }

    public int getBegin() {
        return mBegin;
    }

    public void setBegin(int mBegin) {
        this.mBegin = mBegin;
    }

    public int getEnd() {
        return mEnd;
    }

    public void setEnd(int mEnd) {
        this.mEnd = mEnd;
    }
}