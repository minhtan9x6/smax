package jp.co.primesecurity.tapdoorcloud.mvp.view.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import jp.co.primesecurity.tapdoorcloud.R;
import jp.co.primesecurity.tapdoorcloud.common.AppConst;
import jp.co.primesecurity.tapdoorcloud.mvp.base.baseadapter.BaseRecyclerAdapter;
import jp.co.primesecurity.tapdoorcloud.mvp.base.baseviewholder.BaseItemViewHolder;
import jp.co.primesecurity.tapdoorcloud.mvp.model.data.Tags;
import jp.co.primesecurity.tapdoorcloud.mvp.model.data.Threads;
import jp.co.primesecurity.tapdoorcloud.util.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;


public class PageInboxAdapter extends BaseRecyclerAdapter<Threads, PageInboxAdapter.InboxHolder> {

    private List<Tags> mTags;

    public PageInboxAdapter(Context context) {
        super(context);
        mTags = new ArrayList<>();
    }

    public PageInboxAdapter(Context context, List<Threads> threads, List<Tags> tags) {
        super(context, threads);
        mTags = tags;
    }

    public PageInboxAdapter(Context context, List<Tags> tags) {
        super(context);
        mTags = tags;
    }

    public void setTags(List<Tags> tags) {
        mTags = tags;
    }

    public void updateThread(Threads threads, int position) {
        getDatas().set(position, threads);
        notifyItemChanged(position);
    }

    @Override
    protected int getItemResourceLayout(int viewType) {
        return R.layout.row_list_comment;
    }

    @Override
    public InboxHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = getView(parent, viewType);
        return new InboxHolder(view, mItemClickListener, mTags);
    }

    public static class InboxHolder extends BaseItemViewHolder<Threads> {
        @BindView(R.id.civ_avatar)
        ImageView mCivAvatar;
        @BindView(R.id.civ_last_view)
        ImageView mCivLastView;
        @BindView(R.id.tv_name)
        TextView mTvName;
        @BindView(R.id.tv_time)
        TextView mTvTime;
        @BindView(R.id.iv_reply)
        ImageView mIvReply;
        @BindView(R.id.tv_msg)
        TextView mTvMsg;
        @BindView(R.id.iv_call)
        ImageView mIvCall;
        @BindView(R.id.iv_contact)
        ImageView mIvContact;
        @BindView(R.id.iv_order)
        ImageView mIvOrder;
        @BindView(R.id.ll_tags)
        LinearLayout mLlTags;

        private Context mContext;
        private List<Tags> mTags;

        public InboxHolder(View itemView, OnItemClickListener onItemClickListener, List<Tags> tags) {
            super(itemView, onItemClickListener, null);
            mContext = itemView.getContext();
            mTags = tags;
        }

        @Override
        public void bind(Threads threads, int position) {
            Utils.loadAvatarFb(mCivAvatar, threads.getFbSender().getFbid());
            mCivLastView.setImageResource("comment".equalsIgnoreCase(threads.getType()) ?
                    R.drawable.com_facebook_button_icon_blue : R.drawable.com_facebook_button_send_icon_blue);
            mTvName.setText(threads.getFbSender().getFbname());
            mTvName.setTextColor(ContextCompat.getColor(mContext, threads.getIsnew() ? R.color.colorPrimary : R.color.black));
            // time
            mTvTime.setText(getDate(Long.valueOf(threads.getFbupdatedTime()), AppConst.FORMAT_DATE_BIRTHDAY));
//
            mIvReply.setVisibility(threads.getCountReplied() > 0 ? View.GONE : View.VISIBLE);
            mTvMsg.setText(threads.getFbmsg());
            mIvCall.setVisibility((threads.getPhones() != null && threads.getPhones().size() > 0) ? View.VISIBLE : View.GONE);
            mIvContact.setVisibility((threads.getCustomers() != null && threads.getCustomers().size() > 1) ? View.VISIBLE : View.GONE);
            mIvOrder.setVisibility((threads.getOrders() != null && threads.getOrders().size() > 0) ? View.VISIBLE : View.GONE);
            List<String> tagsList = threads.getTags();
            mLlTags.setVisibility((tagsList != null && tagsList.size() > 0) ? View.VISIBLE : View.INVISIBLE);
            if (tagsList != null && mTags != null && mTags.size() > 0) {
                mLlTags.removeAllViews();
                for (String tagId : tagsList) {
                    Tags tags = getTags(tagId);
                    if (tags != null) {
                        TextView textView = new TextView(mContext);
                        textView.setText(tags.getTitle());
                        textView.setTextColor(Color.parseColor(tags.getBgcolor()));
                        textView.setPadding(10, 10, 10, 10);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        layoutParams.setMarginEnd(10);
                        textView.setLayoutParams(layoutParams);
                        GradientDrawable shape = new GradientDrawable();
                        shape.setShape(GradientDrawable.RECTANGLE);
                        shape.setCornerRadii(new float[]{8, 8, 8, 8, 8, 8, 8, 8});
                        shape.setColor(Color.parseColor(tags.getColor()));
                        textView.setBackground(shape);
                        mLlTags.addView(textView);
                    }
                }
            }
        }

        private Tags getTags(String tagId) {
            for (Tags tags : mTags) {
                if (tags.getId().equalsIgnoreCase(tagId)) {
                    return tags;
                }
            }
            return null;
        }

        public static String getDate(long milliSeconds, String dateFormat) {
            SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(milliSeconds);
            return formatter.format(calendar.getTime());
        }
    }
}

