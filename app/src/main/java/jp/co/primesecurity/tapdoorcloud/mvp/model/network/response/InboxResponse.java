package jp.co.primesecurity.tapdoorcloud.mvp.model.network.response;

import com.google.gson.annotations.SerializedName;

import jp.co.primesecurity.tapdoorcloud.mvp.model.data.DataInbox;

/**
 * Created by thanhvn on 12/17/17.
 */

public class InboxResponse {
    @SerializedName("error")
    private String mError;
    @SerializedName("data")
    private DataInbox mData;

    public String getError() {
        return mError;
    }

    public void setError(String mError) {
        this.mError = mError;
    }

    public DataInbox getData() {
        return mData;
    }

    public void setData(DataInbox mData) {
        this.mData = mData;
    }
}
