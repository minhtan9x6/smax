package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

/**
 * Created by thanhvn on 1/3/18.
 */

public class FbAttachment {
    @SerializedName("url")
    private String mUrl;
    @SerializedName("type")
    private String mType;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("target")
    private Target mTarget;
    @SerializedName("media")
    private Media mMedia;
    @SerializedName("subattachments")
    private SubAttachments mSubAttachments;

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public String getType() {
        return mType;
    }

    public void setType(String mType) {
        this.mType = mType;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public Target getTarget() {
        return mTarget;
    }

    public void setTarget(Target mTarget) {
        this.mTarget = mTarget;
    }

    public SubAttachments getSubAttachments() {
        return mSubAttachments;
    }

    public void setSubAttachments(SubAttachments subattachments) {
        this.mSubAttachments = subattachments;
    }

    public Media getMedia() {
        return mMedia;
    }

    public void setMedia(Media mMedia) {
        this.mMedia = mMedia;
    }
}
