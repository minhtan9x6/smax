package jp.co.primesecurity.tapdoorcloud.mvp.model.network.tasks;

import android.content.Context;

import io.reactivex.Observable;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.PopulationRankResponse;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.ApiServiceInterface;

/**
 * Created by thanh.vn on 6/21/2017.
 */

public class GetBiApiTask extends BaseBiCallbackTask<PopulationRankResponse, PopulationRankResponse> {

    private ApiServiceInterface mApiServiceInterface;
    private String mCountry;

    public GetBiApiTask(Context context, ApiServiceInterface apiServiceInterface, String country) {
        super(context);
        mApiServiceInterface = apiServiceInterface;
        mCountry = country;
    }

    @Override
    public Observable<PopulationRankResponse> processAction1() {
        return mApiServiceInterface.getPopulationRank("1952-03-11", "male", mCountry);
    }

    @Override
    public Observable<PopulationRankResponse> processAction2() {
        return mApiServiceInterface.getPopulationRankOnDate("1952-03-12", "male", mCountry, "2001-05-11");
    }
}
