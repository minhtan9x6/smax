package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by thanhvn on 12/17/17.
 */

public class DataInbox {
    @SerializedName("threads")
    private List<Threads> mThreads;
    @SerializedName("threads_total")
    private int mThreadsTotal;
    @SerializedName("pagination")
    private Pagination mPagination;

    public List<Threads> getThreads() {
        return mThreads;
    }

    public void setThreads(List<Threads> mThreads) {
        this.mThreads = mThreads;
    }

    public int getThreadsTotal() {
        return mThreadsTotal;
    }

    public void setThreadsTotal(int mThreadsTotal) {
        this.mThreadsTotal = mThreadsTotal;
    }

    public Pagination getPagination() {
        return mPagination;
    }

    public void setPagination(Pagination mPagination) {
        this.mPagination = mPagination;
    }
}
