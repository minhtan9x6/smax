package jp.co.primesecurity.tapdoorcloud.mvp.view.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import jp.co.primesecurity.tapdoorcloud.R;

import java.util.ArrayList;

public class SpinnerTypeAdapter extends ArrayAdapter<String> {

    private LayoutInflater mLayoutInflater;
    private String[] mTypes;

    public SpinnerTypeAdapter(Context context, int textViewResourceId, String[] types) {
        super(context, textViewResourceId, types);
        mLayoutInflater = ((Activity) context).getLayoutInflater();
        mTypes = types;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent, true);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent, false);
    }

    private View getCustomView(int position, View convertView, ViewGroup parent, boolean isDropDown) {
        String[] types = mTypes;
        String type = types[position];
        View row = convertView;

        ViewHolder holder = null;
        if (row == null) {
            holder = new ViewHolder();
            row = mLayoutInflater.inflate(R.layout.row_spinner_type, parent, false);
            holder.type = row.findViewById(R.id.tv_type);
            holder.icon = row.findViewById(R.id.iv_icon);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        holder.type.setText(type);
        if (isDropDown) {
            holder.icon.setVisibility(position != 0 ? View.VISIBLE : View.VISIBLE);
            int iconResId = 0;
            switch (position) {
                case 1:
                    iconResId = R.drawable.com_facebook_button_icon_blue;
                    break;
                case 2:
                    iconResId = R.drawable.com_facebook_button_send_icon_blue;
                    break;
                case 3:
                    iconResId = R.drawable.mark_as_unread;
                    break;
            }
            holder.icon.setImageResource(iconResId);
        } else {
            holder.icon.setVisibility(View.GONE);
        }
        return row;
    }

    static class ViewHolder {
        TextView type;
        ImageView icon;
    }
}