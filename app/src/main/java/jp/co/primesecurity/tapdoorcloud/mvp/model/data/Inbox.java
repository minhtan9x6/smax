package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

public class Inbox {
    @SerializedName("mode_last")
    private int mModeLast;
    @SerializedName("mode_token")
    private int mModeToken;
    @SerializedName("format")
    private int mFormat;
    @SerializedName("time")
    private Time mTime;
    @SerializedName("mode_range")
    private ModeRange mModeRange;

    public int getModeLast() {
        return mModeLast;
    }

    public void setModeLast(int mModeLast) {
        this.mModeLast = mModeLast;
    }

    public int getModeToken() {
        return mModeToken;
    }

    public void setModeToken(int mModeToken) {
        this.mModeToken = mModeToken;
    }

    public int getFormat() {
        return mFormat;
    }

    public void setFormat(int mFormat) {
        this.mFormat = mFormat;
    }

    public Time getTime() {
        return mTime;
    }

    public void setTime(Time time) {
        this.mTime = time;
    }

    public ModeRange getModeRange() {
        return mModeRange;
    }

    public void setModeRange(ModeRange mModeRange) {
        this.mModeRange = mModeRange;
    }
}