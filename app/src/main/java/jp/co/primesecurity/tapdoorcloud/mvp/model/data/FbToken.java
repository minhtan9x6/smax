package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FbToken {

    @SerializedName("fbid")
    @Expose
    private String fbid;
    @SerializedName("fbname")
    @Expose
    private String fbname;
    @SerializedName("value")
    @Expose
    private String value;

    public String getFbid() {
        return fbid;
    }

    public void setFbid(String fbid) {
        this.fbid = fbid;
    }

    public String getFbname() {
        return fbname;
    }

    public void setFbname(String fbname) {
        this.fbname = fbname;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}