package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubAttachments {
    @SerializedName("data")
    private List<DataSubAttachment> mData;

    public List<DataSubAttachment> getData() {
        return mData;
    }

    public void setData(List<DataSubAttachment> mData) {
        this.mData = mData;
    }
}