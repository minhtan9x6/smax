package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

public class ImageData {
    @SerializedName("render_as_sticker")
    private boolean mRenderAsSticker;
    @SerializedName("image_type")
    private int mImageType;
    @SerializedName("preview_url")
    private String mPreviewUrl;
    @SerializedName("url")
    private String mUrl;
    @SerializedName("max_height")
    private int mMaxHeight;
    @SerializedName("max_width")
    private int mMaxWidth;
    @SerializedName("height")
    private int mHeight;
    @SerializedName("width")
    private int mWidth;

    public boolean getRenderAsSticker() {
        return mRenderAsSticker;
    }

    public void setRenderAsSticker(boolean mRenderAsSticker) {
        this.mRenderAsSticker = mRenderAsSticker;
    }

    public int getImageType() {
        return mImageType;
    }

    public void setImageType(int mImageType) {
        this.mImageType = mImageType;
    }

    public String getPreviewUrl() {
        return mPreviewUrl;
    }

    public void setPreviewUrl(String mPreviewUrl) {
        this.mPreviewUrl = mPreviewUrl;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public int getMaxHeight() {
        return mMaxHeight;
    }

    public void setMaxHeight(int mMaxHeight) {
        this.mMaxHeight = mMaxHeight;
    }

    public int getMaxWidth() {
        return mMaxWidth;
    }

    public void setMaxWidth(int mMaxWidth) {
        this.mMaxWidth = mMaxWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setHeight(int mHeight) {
        this.mHeight = mHeight;
    }

    public int getWidth() {
        return mWidth;
    }

    public void setWidth(int mWidth) {
        this.mWidth = mWidth;
    }
}