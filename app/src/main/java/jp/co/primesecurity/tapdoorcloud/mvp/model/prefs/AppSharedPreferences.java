package jp.co.primesecurity.tapdoorcloud.mvp.model.prefs;

import android.content.SharedPreferences;
import android.util.Base64;

import com.blankj.utilcode.util.EncodeUtils;

/**
 * Created by thanh.vn on 2017/01/15.
 */

public class AppSharedPreferences {

    public static final String ACCOUNT = "Account";
    public static final String ACCOUNT_ID = "Account ID";
    public static final String PASSWORD = "Password";
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String REFRESH_TOKEN = "REFRESH_TOKEN";
    public static final String CAPTCHA_TOKEN = "captcha_token";
    public static final String EXPIRES_IN = "expires_in";
    public static final String LOGIN_SUCCESS = "Login success";
    public static final String IS_USER_UPDATED = "Is user updated";
    public static final String FULL_NAME = "FullName";
    public static final String REFRESHING = "must refresh token";
    public static final String CHECK_SHOW_RECHARGE = "check show recharge";

    public static final String FACEBOOK_FIRST_NAME = "fb first name";
    public static final String FACEBOOK_LAST_NAME = "fb last name";
    public static final String FACEBOOK_GENDER = "fb gender";
    public static final String FACEBOOK_BIRTHDAY = "fb birthday";
    public static final String USER_DATA = "user data";
    public static final String IS_NEED_VIEW_TUTORIAL = "need_view_tutorial";
    public static final String LINK_FACEBOOK = "facebook";
    public static final String LINK_TWITTER = "twitter";
    public static final String LINK_INSTAGRAM = "instagram";
    public static final String LINK_OTHER_1 = "other1";
    public static final String LINK_OTHER_2 = "other2";
    public static final String NUMBER_OF_PET = "number of pet";

    private String mAccount;
    private String mAccountId;
    private String mFullName;
    private String mPassword;
    private String mCaptchaToken;
    private String mAccessToken;
    private String mRefreshToken;
    private Long mExpiresIn;
    private boolean mCheckLoginSuccess;
    private boolean mIsRefresh;
    private boolean mIsShowRecharge;
    private boolean mIsUserUpdated;

    private String mFbFirstName;
    private String mFbLastName;
    private String mFbGender;
    private String mFbBirthday;
    private boolean mIsNeedViewTutorial;
    private int mNumberOfPet;

    private AppSharedPreferences(SharedPreferences pPref) {
        mAccount = pPref.getString(ACCOUNT, "");
        mAccountId = pPref.getString(ACCOUNT_ID, "");
        mFullName = pPref.getString(FULL_NAME, "");
        mPassword = pPref.getString(PASSWORD, "");
        mAccessToken = pPref.getString(ACCESS_TOKEN, "");
        mRefreshToken = pPref.getString(REFRESH_TOKEN, "");
        mCaptchaToken = pPref.getString(CAPTCHA_TOKEN, "");
        mExpiresIn = pPref.getLong(EXPIRES_IN, 0);
        mCheckLoginSuccess = pPref.getBoolean(LOGIN_SUCCESS, false);
        mIsRefresh = pPref.getBoolean(REFRESHING, false);
        mIsShowRecharge = pPref.getBoolean(CHECK_SHOW_RECHARGE, false);
        mIsUserUpdated = pPref.getBoolean(IS_USER_UPDATED, false);
        mIsNeedViewTutorial = pPref.getBoolean(IS_NEED_VIEW_TUTORIAL, true);
        //Facebook info
        mFbFirstName = pPref.getString(FACEBOOK_FIRST_NAME, "");
        mFbLastName = pPref.getString(FACEBOOK_LAST_NAME, "");
        mFbGender = pPref.getString(FACEBOOK_GENDER, "");
        mFbBirthday = pPref.getString(FACEBOOK_BIRTHDAY, "");
        mNumberOfPet = pPref.getInt(NUMBER_OF_PET, 0);
    }

    private static AppSharedPreferences sMAppSharedPreferences;

    public static AppSharedPreferences getInstance(SharedPreferences pPref) {
        if (sMAppSharedPreferences == null) {
            sMAppSharedPreferences = new AppSharedPreferences(pPref);
        }
        return sMAppSharedPreferences;
    }

    public void setAppSharedPreferences(SharedPreferences pPref) {
        SharedPreferences.Editor editor = pPref.edit();
        editor.putString(ACCOUNT, EncodeUtils.base64Encode2String(mAccount.getBytes()));
        editor.putString(ACCOUNT_ID, mAccountId);
        editor.putString(FULL_NAME, EncodeUtils.base64Encode2String(mFullName.getBytes()));
        editor.putString(PASSWORD, EncodeUtils.base64Encode2String(mPassword.getBytes()));
        editor.putString(ACCESS_TOKEN, EncodeUtils.base64Encode2String(mAccessToken.getBytes()));
        editor.putString(REFRESH_TOKEN, mRefreshToken);
        editor.putString(CAPTCHA_TOKEN, mCaptchaToken);
        editor.putLong(EXPIRES_IN, mExpiresIn);
        editor.putBoolean(LOGIN_SUCCESS, mCheckLoginSuccess);
        editor.putBoolean(IS_USER_UPDATED, mIsUserUpdated);
        editor.putBoolean(REFRESHING, mIsRefresh);
        editor.putBoolean(CHECK_SHOW_RECHARGE, mIsShowRecharge);
        editor.putString(FACEBOOK_FIRST_NAME, mFbFirstName);
        editor.putString(FACEBOOK_LAST_NAME, mFbLastName);
        editor.putString(FACEBOOK_GENDER, mFbGender);
        editor.putString(FACEBOOK_BIRTHDAY, mFbBirthday);
        editor.putBoolean(IS_NEED_VIEW_TUTORIAL, mIsNeedViewTutorial);
        editor.putInt(NUMBER_OF_PET, mNumberOfPet);
        editor.apply();
    }

    private static String encrypt(String input) {
        return Base64.encodeToString(input.getBytes(), Base64.DEFAULT);
    }

    private static String decrypt(String input) {
        if (input != null) {
            return new String(Base64.decode(input, Base64.DEFAULT));
        }
        return null;
    }

    public int getNumberOfPet() {
        return mNumberOfPet;
    }

    public void setNumberOfPet(int mNumberOfPet) {
        this.mNumberOfPet = mNumberOfPet;
    }

    public String getAccessToken() {
        return new String(EncodeUtils.base64Decode(mAccessToken));
    }

    public void setAccessToken(String mAccessToken) {
        this.mAccessToken = EncodeUtils.base64Encode2String(mAccessToken.getBytes());
    }

    public String getRefreshToken() {
        return mRefreshToken;
    }

    public void setRefreshToken(String mRefreshToken) {
        this.mRefreshToken = mRefreshToken;
    }

    public String getCaptchaToken() {
        return mCaptchaToken;
    }

    public void setCaptchaToken(String mCaptchaToken) {
        this.mCaptchaToken = mCaptchaToken;
    }

    public String getAccount() {
        return mAccount;
    }

    public void setAccount(String mAccount) {
        this.mAccount = mAccount;
    }

    public String getAccountId() {
        return mAccountId;
    }

    public void setAccountId(String accountId) {
        mAccountId = accountId;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public Long getExpiresIn() {
        return mExpiresIn;
    }

    public void setExpiresIn(Long mExpiresIn) {
        this.mExpiresIn = mExpiresIn;
    }

    public boolean isCheckLoginSuccess() {
        return mCheckLoginSuccess;
    }

    public void setCheckLoginSuccess(boolean mCheckLoginSuccess) {
        this.mCheckLoginSuccess = mCheckLoginSuccess;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String mFullName) {
        this.mFullName = mFullName;
    }

    public boolean isRefresh() {
        return mIsRefresh;
    }

    public void setRefresh(boolean mIsRefresh) {
        this.mIsRefresh = mIsRefresh;
    }

    public boolean isShowRecharge() {
        return mIsShowRecharge;
    }

    public void setIsShowRecharge(boolean mIsShowRecharge) {
        this.mIsShowRecharge = mIsShowRecharge;
    }

    public boolean isUserUpdated() {
        return mIsUserUpdated;
    }

    public void setUserUpdated(boolean mIsLoginFirstTime) {
        this.mIsUserUpdated = mIsLoginFirstTime;
    }

    public String getFbFirstName() {
        return mFbFirstName;
    }

    public void setFbFirstName(String mFbFirstName) {
        this.mFbFirstName = mFbFirstName;
    }

    public String getFbLastName() {
        return mFbLastName;
    }

    public void setFbLastName(String mFbLastName) {
        this.mFbLastName = mFbLastName;
    }

    public String getFbGender() {
        return mFbGender;
    }

    public void setFbGender(String mFbGender) {
        this.mFbGender = mFbGender;
    }

    public String getFbBirthday() {
        return mFbBirthday;
    }

    public void setFbBirthday(String mFbBirthday) {
        this.mFbBirthday = mFbBirthday;
    }

    public boolean isNeedViewTutorial() {
        return mIsNeedViewTutorial;
    }

    public void setIsNeedViewTutorial(boolean mIsNeedViewTutorial) {
        this.mIsNeedViewTutorial = mIsNeedViewTutorial;
    }
}
