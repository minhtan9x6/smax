package jp.co.primesecurity.tapdoorcloud.mvp.model.network.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by thanh.vn on 6/9/2017.
 */

public class PopulationRankResponse {
    @SerializedName("dob")
    private String mDob;
    @SerializedName("country")
    private String mCountry;
    @SerializedName("date")
    private String mDate;
    @SerializedName("rank")
    private int mRank;
    @SerializedName("sex")
    private String mSex;

    public String getDob() {
        return mDob;
    }

    public void setDob(String mDob) {
        this.mDob = mDob;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String mCountry) {
        this.mCountry = mCountry;
    }

    public int getRank() {
        return mRank;
    }

    public void setRank(int mRank) {
        this.mRank = mRank;
    }

    public String getSex() {
        return mSex;
    }

    public void setSex(String mSex) {
        this.mSex = mSex;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }
}
