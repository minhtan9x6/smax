package jp.co.primesecurity.tapdoorcloud.mvp.model.network.response;

import com.google.gson.annotations.SerializedName;

import jp.co.primesecurity.tapdoorcloud.mvp.model.data.DataDetailFbPage;

/**
 * Created by thanhvn on 12/17/17.
 */

public class DetailFbPageResponse {
    @SerializedName("data")
    private DataDetailFbPage mData;

    public DataDetailFbPage getData() {
        return mData;
    }

    public void setData(DataDetailFbPage mData) {
        this.mData = mData;
    }
}
