package jp.co.primesecurity.tapdoorcloud.mvp.presenter;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;

import jp.co.primesecurity.tapdoorcloud.R;
import jp.co.primesecurity.tapdoorcloud.mvp.base.BasePresenter;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.Repository;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.tasks.BaseCallback;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.tasks.GetRepositoryTask;
import jp.co.primesecurity.tapdoorcloud.mvp.view.mvpview.MainMvpView;
import okhttp3.Headers;


public class MainPresenter extends BasePresenter<MainMvpView> {

    public static String TAG = "MainPresenter";

    private CompositeDisposable mCompositeDisposable;
    private List<Repository> mRepositories;

    @Override
    public void attachView(MainMvpView view) {
        super.attachView(view);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }
    }

//    public void loadRepositories(String usernameEntered) {
//        String username = usernameEntered.trim();
//        if (username.isEmpty()) return;
//        mCompositeDisposable = new CompositeDisposable();
//        getMvpView().showProgressIndicator();
//        GetRepositoryTask getRepositoryTask = new GetRepositoryTask(getContext(), username);
//        mCompositeDisposable.add(getRepositoryTask.observable(new BaseCallback.OnFinishCallbackListener() {
//            @Override
//            public void onSuccess(Object pResult, Headers headers) {
//                mRepositories = (List<Repository>) pResult;
//                if (mRepositories != null) {
//                    if (!mRepositories.isEmpty()) {
//                        getMvpView().showRepositories(mRepositories);
//                    } else {
//                        getMvpView().showMessage(R.string.text_empty_repos);
//                    }
//                }
//            }
//
//            @Override
//            public void onError(Object error, Headers headers) {
//                getMvpView().showMessage(R.string.error_loading_repos);
//            }
//
//            @Override
//            public void onFailed(Integer pResult) {
//
//            }
//        }));
//    }
}