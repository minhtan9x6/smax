package jp.co.primesecurity.tapdoorcloud.mvp.model.network.tasks;

import android.content.Context;

import io.reactivex.Observable;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.request.ThreadRequest;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.InboxResponse;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.AppService;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.tasks.BaseCallbackTask;

/**
 * Created by thanh.vn on 6/21/2017.
 */

public class PostInboxThreadTask extends BaseCallbackTask<InboxResponse> {

    private String mFbId;
    private ThreadRequest mThreadRequest;

    public PostInboxThreadTask(Context context, String fbId, ThreadRequest threadRequest) {
        super(context, true);
        mFbId = fbId;
        mThreadRequest = threadRequest;
    }

    @Override
    protected Observable<InboxResponse> processAction() {
        return AppService.getInstance().createServiceWithAccessToken(getContext()).postInboxThread(mFbId, mThreadRequest);
    }
}
