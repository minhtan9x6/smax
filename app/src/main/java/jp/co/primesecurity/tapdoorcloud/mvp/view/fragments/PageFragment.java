package jp.co.primesecurity.tapdoorcloud.mvp.view.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.SearchView;
import android.widget.Spinner;

import jp.co.primesecurity.tapdoorcloud.R;
import jp.co.primesecurity.tapdoorcloud.common.AppConst;
import jp.co.primesecurity.tapdoorcloud.mvp.base.BaseFragment;
import jp.co.primesecurity.tapdoorcloud.mvp.base.baseadapter.BaseRecyclerAdapter;
import jp.co.primesecurity.tapdoorcloud.mvp.model.data.Filter;
import jp.co.primesecurity.tapdoorcloud.mvp.model.data.Tags;
import jp.co.primesecurity.tapdoorcloud.mvp.model.data.Threads;
import jp.co.primesecurity.tapdoorcloud.mvp.presenter.PageInboxPresenter;
import jp.co.primesecurity.tapdoorcloud.mvp.view.activities.MainActivity;
import jp.co.primesecurity.tapdoorcloud.mvp.view.adapters.SpinnerTypeAdapter;
import jp.co.primesecurity.tapdoorcloud.mvp.view.adapters.PageInboxAdapter;
import jp.co.primesecurity.tapdoorcloud.mvp.view.mvpview.PageInboxMvpView;
import com.blankj.utilcode.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnItemSelected;

/**
 * A simple {@link Fragment} subclass.
 */
public class PageFragment extends BaseFragment implements PageInboxMvpView {

    @BindView(R.id.rv_chat)
    RecyclerView mRvPageInbox;
    @BindView(R.id.sp_type)
    Spinner mSpType;
    @BindView(R.id.sp_tag)
    Spinner mSpTag;
    @BindView(R.id.searchView)
    SearchView mSearchView;
    private PageInboxPresenter mPageInboxPresenter;
    private String mFbId;
    private List<Tags> mTags = new ArrayList<>();
    private Filter mFilter;

    private PageInboxAdapter mPageInboxAdapter;
    private ArrayAdapter<String> mTagsAdapter;
    private int mCheck;
    private int mPositionChat;

    public static PageFragment newInstance(String fbId) {
        PageFragment pageFragment = new PageFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AppConst.FB_ID, fbId);
        pageFragment.setArguments(bundle);
        return pageFragment;
    }

    @Override
    protected int getContentResId() {
        return R.layout.fragment_page;
    }

    @Override
    protected void init() {
        Bundle bundle = getArguments();
        mPositionChat = -1;
        mFilter = new Filter();
        mPageInboxPresenter = new PageInboxPresenter();
        mPageInboxPresenter.attachView(this);
//        if (bundle != null) {
//            mFbId = bundle.getString(AppConst.FB_ID);
//            loadPageInfo(true);
//            if (getActivity() instanceof MainActivity) {
//                ((MainActivity) getActivity()).subscribe(mFbId);
//            }
//            mPageInboxPresenter.listenEvent(getActivity());
//        }



        // Search keyword
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                mFilter.setKeyWord(s);
                mPageInboxPresenter.loadThreads(mFbId, mFilter);
                mSearchView.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        // Spinner Tags
        List<String> listTags = new ArrayList<>();
        listTags.add(getString(R.string.tag));
        mTagsAdapter = new ArrayAdapter<String>(getMyContext(), android.R.layout.simple_spinner_item, listTags);
        mTagsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpTag.setAdapter(mTagsAdapter);
        mSpTag.setSelection(0, false);
        // Spinner Type
        SpinnerTypeAdapter typeAdapter = new SpinnerTypeAdapter(getMyContext(), R.layout.row_spinner_type, getResources().getStringArray(R.array.type));
        mSpType.setAdapter(typeAdapter);
        mSpType.setSelection(0, false);

        // recyclerview Inbox
        mRvPageInbox.setLayoutManager(new LinearLayoutManager(getMyContext()));
        mRvPageInbox.setItemAnimator(new DefaultItemAnimator());
        mRvPageInbox.setItemViewCacheSize(15);
        mRvPageInbox.setDrawingCacheEnabled(true);
        mRvPageInbox.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mPageInboxAdapter = new PageInboxAdapter(getActivity());
        mPageInboxAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
//                mPositionChat = position;
//                Threads threads = mPageInboxAdapter.getDatas().get(position);
//                showDialogFragmentFullScreen(InboxDialogFragment.newInstance(threads.getFbid(), threads.getTypeParent()));
            }
        });
        loadPageInfo(false);
        mRvPageInbox.setAdapter(mPageInboxAdapter);
    }

    public void loadPageInfo(boolean isLoading) {
        mPageInboxPresenter.loadPageInfo(isLoading, mFbId, mFilter);
    }

    @Override
    public Context getMyContext() {
        return getContext();
    }

    @Override
    public void showInfoPage(List<Threads> threads, List<Tags> tags) {
        mTags = tags;
        mPageInboxAdapter.setTags(tags);
        mPageInboxAdapter.addAll(threads);
        mPageInboxAdapter.notifyDataSetChanged();
        // Spinner Tags
        for (Tags tag : tags) {
            mTagsAdapter.add(tag.getTitle());
        }
        mTagsAdapter.notifyDataSetChanged();
    }

    @Override
    public void showThreads(List<Threads> threads) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mPageInboxAdapter.addNewAll(threads);
            }
        });
    }

    @Override
    public void updateThreadNew(Threads threads) {
        if (mPageInboxAdapter != null && mPositionChat >= 0) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mPageInboxAdapter.updateThread(threads, mPositionChat);
                }
            });
        }
    }

    @OnItemSelected(R.id.sp_tag)
    void onSelectedTags(Spinner spinner, int position) {
        if (position == 0) {
            mFilter.setTagId(null);
        } else {
            // position >= 1
            if (mTags != null) {
                mFilter.setTagId(mTags.get(position - 1).getId());
            }
        }
        if (spinner.getAdapter().getCount() > 1) {
//            LogUtils.d("SP_TAG");
            mPageInboxPresenter.loadThreads(mFbId, mFilter);
        }
    }

    @OnItemSelected(R.id.sp_type)
    void onSelectedTypes(Spinner spinner, int position) {
//        LogUtils.d("SP_TYPE");
        if (++mCheck > 1) {
            switch (position) {
                case 0:
                    mFilter.setComment(false);
                    mFilter.setInbox(false);
                    mFilter.setIsnew(false);
                    break;
                case 1:
                    mFilter.setComment(true);
                    mFilter.setInbox(false);
                    mFilter.setIsnew(false);
                    break;
                case 2:
                    mFilter.setComment(false);
                    mFilter.setInbox(true);
                    mFilter.setIsnew(false);
                    break;
                case 3:
                    mFilter.setComment(false);
                    mFilter.setInbox(false);
                    mFilter.setIsnew(true);
                    break;
            }
            mPageInboxPresenter.loadThreads(mFbId, mFilter);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPageInboxPresenter.detachView();
    }
}