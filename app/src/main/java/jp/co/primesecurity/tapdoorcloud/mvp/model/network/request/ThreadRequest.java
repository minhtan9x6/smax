package jp.co.primesecurity.tapdoorcloud.mvp.model.network.request;

import com.google.gson.annotations.SerializedName;

import jp.co.primesecurity.tapdoorcloud.mvp.model.data.Filter;

/**
 * Created by thanhvn on 12/17/17.
 */

public class ThreadRequest {
    @SerializedName("page_current")
    private int mPageCurrent;
    @SerializedName("filter")
    private Filter mFilter;

    public ThreadRequest() {
        mPageCurrent = 1;
    }

    public ThreadRequest(Filter filter) {
        mPageCurrent = 1;
        mFilter = filter;
    }

    public int getPageCurrent() {
        return mPageCurrent;
    }

    public void setPageCurrent(int mPageCurrent) {
        this.mPageCurrent = mPageCurrent;
    }

    public Filter getFilter() {
        return mFilter;
    }

    public void setFilter(Filter mFilter) {
        this.mFilter = mFilter;
    }
}
