package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by thanhvn on 12/17/17.
 */

public class DataDetailFbPage {
    @SerializedName("_id")
    private String mId;
    @SerializedName("updated_time")
    private String mUpdatedTime;
    @SerializedName("created_time")
    private String mCreatedTime;
    @SerializedName("fbfan_count")
    private int mFbfanCount;
    @SerializedName("fblink")
    private String mFblink;
    @SerializedName("fbabout")
    private String mFbabout;
    @SerializedName("fbpicture")
    private String mFbpicture;
    @SerializedName("fbname")
    private String mFbname;
    @SerializedName("fbid")
    private String mFbid;
    @SerializedName("__v")
    private int mV;
    @SerializedName("lastview")
    private LastView mLastView;
    @SerializedName("autoreply")
    private Autoreply mAutoreply;
    @SerializedName("statistics")
    private Statistics mStatistics;
    @SerializedName("subscribed")
    private Subscribed mSubscribed;
    @SerializedName("active")
    private Active mActive;
    @SerializedName("crawl")
    private Crawl mCrawl;
    @SerializedName("message_sample")
    private List<MessageSample> mMessageSample;
    @SerializedName("shares")
    private List<Shares> mShares;
    @SerializedName("fbtoken")
    private FbToken mFbtoken;
    @SerializedName("admins")
    private List<Admins> mAdmins;
    @SerializedName("config")
    private Config mConfig;
    @SerializedName("tags")
    private List<Tags> mTags;

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getUpdatedTime() {
        return mUpdatedTime;
    }

    public void setUpdatedTime(String mUpdatedTime) {
        this.mUpdatedTime = mUpdatedTime;
    }

    public String getCreatedTime() {
        return mCreatedTime;
    }

    public void setCreatedTime(String mCreatedTime) {
        this.mCreatedTime = mCreatedTime;
    }

    public int getFbfanCount() {
        return mFbfanCount;
    }

    public void setFbfanCount(int mFbfanCount) {
        this.mFbfanCount = mFbfanCount;
    }

    public String getFblink() {
        return mFblink;
    }

    public void setFblink(String mFblink) {
        this.mFblink = mFblink;
    }

    public String getFbabout() {
        return mFbabout;
    }

    public void setFbabout(String mFbabout) {
        this.mFbabout = mFbabout;
    }

    public String getFbpicture() {
        return mFbpicture;
    }

    public void setFbpicture(String mFbpicture) {
        this.mFbpicture = mFbpicture;
    }

    public String getFbname() {
        return mFbname;
    }

    public void setFbname(String mFbname) {
        this.mFbname = mFbname;
    }

    public String getFbid() {
        return mFbid;
    }

    public void setFbid(String mFbid) {
        this.mFbid = mFbid;
    }

    public int getV() {
        return mV;
    }

    public void setV(int mV) {
        this.mV = mV;
    }

    public LastView getLastView() {
        return mLastView;
    }

    public void setLastView(LastView mLastView) {
        this.mLastView = mLastView;
    }

    public Autoreply getAutoreply() {
        return mAutoreply;
    }

    public void setAutoreply(Autoreply mAutoreply) {
        this.mAutoreply = mAutoreply;
    }

    public Statistics getStatistics() {
        return mStatistics;
    }

    public void setStatistics(Statistics mStatistics) {
        this.mStatistics = mStatistics;
    }

    public Subscribed getSubscribed() {
        return mSubscribed;
    }

    public void setSubscribed(Subscribed mSubscribed) {
        this.mSubscribed = mSubscribed;
    }

    public Active getActive() {
        return mActive;
    }

    public void setActive(Active mActive) {
        this.mActive = mActive;
    }

    public Crawl getCrawl() {
        return mCrawl;
    }

    public void setCrawl(Crawl mCrawl) {
        this.mCrawl = mCrawl;
    }

    public List<MessageSample> getMessageSample() {
        return mMessageSample;
    }

    public void setMessageSample(List<MessageSample> mMessageSample) {
        this.mMessageSample = mMessageSample;
    }

    public List<Shares> getShares() {
        return mShares;
    }

    public void setShares(List<Shares> mShares) {
        this.mShares = mShares;
    }

    public FbToken getFbToken() {
        return mFbtoken;
    }

    public void setFbToken(FbToken mFbtoken) {
        this.mFbtoken = mFbtoken;
    }

    public List<Admins> getAdmins() {
        return mAdmins;
    }

    public void setAdmins(List<Admins> mAdmins) {
        this.mAdmins = mAdmins;
    }

    public Config getConfig() {
        return mConfig;
    }

    public void setConfig(Config mConfig) {
        this.mConfig = mConfig;
    }

    public List<Tags> getTags() {
        return mTags;
    }

    public void setTags(List<Tags> mTags) {
        this.mTags = mTags;
    }
}
