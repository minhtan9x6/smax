package jp.co.primesecurity.tapdoorcloud.mvp.model.network.tasks;

import android.content.Context;

import java.util.List;

import io.reactivex.Observable;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.Repository;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.ApiServiceInterface;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.AppService;


/**
 * Created by thanh.vn on 6/21/2017.
 */

public class GetRepositoryTask extends BaseCallbackTask<List<Repository>> {

    private ApiServiceInterface mApiServiceInterface;
    private String mUserName;

    public GetRepositoryTask(Context context, String userName) {
        super(context);
        mUserName = userName;
    }

    @Override
    protected Observable<List<Repository>> processAction() {
        return AppService.getInstance().createService(getContext()).getRepositories(mUserName);
    }
}
