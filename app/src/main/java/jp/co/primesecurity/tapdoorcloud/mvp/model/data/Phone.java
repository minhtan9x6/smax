
package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

public class Phone {
    @SerializedName("none")
    private None mNone;
    @SerializedName("exist")
    private Exist mExist;

    public None getNone() {
        return mNone;
    }

    public void setNone(None mNone) {
        this.mNone = mNone;
    }

    public Exist getExist() {
        return mExist;
    }

    public void setExist(Exist mExist) {
        this.mExist = mExist;
    }
}