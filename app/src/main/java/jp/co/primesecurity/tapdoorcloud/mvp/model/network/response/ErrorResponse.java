package jp.co.primesecurity.tapdoorcloud.mvp.model.network.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by thanh.vn on 6/2/2017.
 */

public class ErrorResponse {
    @SerializedName("detail")
    private String mDetail;

    public String getDetail() {
        return mDetail;
    }

    public void setDetail(String mDetail) {
        this.mDetail = mDetail;
    }
}
