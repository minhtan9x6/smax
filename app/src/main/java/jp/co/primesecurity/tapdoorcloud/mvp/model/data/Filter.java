package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Filter {
    @SerializedName("postid")
    private String mPostId;
    @SerializedName("time")
    private Time mTime;
    @SerializedName("page")
    private int mPage;
    @SerializedName("orders")
    private List<Orders> mOrders;
    @SerializedName("comment")
    private boolean mComment;
    @SerializedName("inbox")
    private boolean mInbox;
    @SerializedName("isnew")
    private boolean mIsnew;
    @SerializedName("replied")
    private boolean mReplied;
    @SerializedName("has_phone")
    private boolean mHasPhone;
    @SerializedName("users")
    private boolean mUsers;
    @SerializedName("tagid")
    private String mTagId;
    @SerializedName("keyword")
    private String mKeyWord;

    public Filter(String postId, Time time, int page, List<Orders> orders, boolean comment, boolean inbox,
                  boolean isnew, boolean replied, boolean hasPhone, boolean users) {
        mPostId = postId;
        mTime = time;
        mPage = page;
        mOrders = orders;
        mComment = comment;
        mInbox = inbox;
        mIsnew = isnew;
        mReplied = replied;
        mHasPhone = hasPhone;
        mUsers = users;
    }

    public Filter(boolean comment, boolean inbox, boolean isnew, boolean replied, boolean hasPhone, boolean users) {
        mTime = new Time(0, 0);
        mPage = 1;
        mComment = comment;
        mInbox = inbox;
        mIsnew = isnew;
        mReplied = replied;
        mHasPhone = hasPhone;
        mUsers = users;
    }

    public Filter() {
        mTime = new Time(0, 0);
        mPage = 1;
    }

    public Filter(boolean comment, boolean inbox, boolean isnew, String tagId) {
        mTime = new Time(0, 0);
        mPage = 1;
        mComment = comment;
        mInbox = inbox;
        mIsnew = isnew;
        mTagId = tagId;
    }

    public String getPostId() {
        return mPostId;
    }

    public void setPostId(String mPostId) {
        this.mPostId = mPostId;
    }

    public Time getTime() {
        return mTime;
    }

    public void setTime(Time mTime) {
        this.mTime = mTime;
    }

    public int getPage() {
        return mPage;
    }

    public void setPage(int mPage) {
        this.mPage = mPage;
    }

    public List<Orders> getOrders() {
        return mOrders;
    }

    public void setOrders(List<Orders> mOrders) {
        this.mOrders = mOrders;
    }

    public boolean getComment() {
        return mComment;
    }

    public void setComment(boolean mComment) {
        this.mComment = mComment;
    }

    public boolean getInbox() {
        return mInbox;
    }

    public void setInbox(boolean mInbox) {
        this.mInbox = mInbox;
    }

    public boolean getIsnew() {
        return mIsnew;
    }

    public void setIsnew(boolean mIsnew) {
        this.mIsnew = mIsnew;
    }

    public boolean getReplied() {
        return mReplied;
    }

    public void setReplied(boolean mReplied) {
        this.mReplied = mReplied;
    }

    public boolean getHasPhone() {
        return mHasPhone;
    }

    public void setHasPhone(boolean mHasPhone) {
        this.mHasPhone = mHasPhone;
    }

    public boolean getUsers() {
        return mUsers;
    }

    public void setUsers(boolean mUsers) {
        this.mUsers = mUsers;
    }

    public String getTagId() {
        return mTagId;
    }

    public void setTagId(String tagId) {
        mTagId = tagId;
    }

    public String getKeyWord() {
        return mKeyWord;
    }

    public void setKeyWord(String keyWord) {
        mKeyWord = keyWord;
    }
}