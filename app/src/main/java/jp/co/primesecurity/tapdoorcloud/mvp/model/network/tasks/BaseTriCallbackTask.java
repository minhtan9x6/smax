package jp.co.primesecurity.tapdoorcloud.mvp.model.network.tasks;

import android.content.Context;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;

/**
 * Created by thanh.vn on 6/21/2017.
 */

public abstract class BaseTriCallbackTask<T1, T2, T3> extends BaseCallback{

    public BaseTriCallbackTask(Context activity, boolean showLoading) {
        super(activity, showLoading);
    }

    public BaseTriCallbackTask(Context activity) {
        super(activity, true);
    }

    public abstract Observable<T1> processAction1();
    public abstract Observable<T2> processAction2();
    public abstract Observable<T3> processAction3();

    private OnCombineCallbackListener mOnCombineCallbackListener;

    private Flowable flowable(Observable observable) {
        return processSubscribe(observable).toFlowable(BackpressureStrategy.BUFFER);
    }


    public interface OnCombineCallbackListener {
        void combineResult(Object result1, Object result2, Object result3);
    }

    public void setOnCombineCallbackListener(OnCombineCallbackListener onCombineCallbackListener) {
        mOnCombineCallbackListener = onCombineCallbackListener;
        Flowable.zip(flowable(processAction1()), flowable(processAction2()), flowable(processAction3()), (o1, o2, o3) -> {
            if (mOnCombineCallbackListener != null) {mOnCombineCallbackListener.combineResult(o1, o2, o3);}
            return "";
        }).subscribe();
    }
}