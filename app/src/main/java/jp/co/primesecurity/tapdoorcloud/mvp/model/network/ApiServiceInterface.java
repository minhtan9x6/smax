package jp.co.primesecurity.tapdoorcloud.mvp.model.network;




import java.util.List;

import io.reactivex.Observable;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.request.ThreadRequest;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.CountriesResponse;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.DetailFbPageResponse;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.InboxResponse;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.PopulationRankResponse;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.Repository;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiServiceInterface {

    @GET("users/{username}/repos")
    Observable<List<Repository>> getRepositories(@Path("username") String username);

    @GET("countries")
    Observable<CountriesResponse> getCountries();

    @GET("wp-rank/{dob}/{sex}/{country}/today/")
    Observable<PopulationRankResponse> getPopulationRank(@Path("dob") String dob, @Path("sex") String sex, @Path("country") String country);

    @GET("wp-rank/{dob}/{sex}/{country}/on/{date}")
    Observable<PopulationRankResponse> getPopulationRankOnDate(@Path("dob") String dob, @Path("sex") String sex, @Path("country") String country, @Path("date") String date);

    Observable<DetailFbPageResponse> getDetailFbPage(String fbId);

    Observable<InboxResponse> postInboxThread(String fbId, ThreadRequest threadRequest);
}
