package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

public class Active {
    @SerializedName("time_start")
    private String mTimeStart;
    @SerializedName("time_end")
    private String mTimeEnd;
    @SerializedName("user_fbid")
    private String mUserFbid;
    @SerializedName("user_name")
    private String mUserName;
    @SerializedName("user_sid")
    private int mUserSid;

    public String getTimeStart() {
        return mTimeStart;
    }

    public void setTimeStart(String mTimeStart) {
        this.mTimeStart = mTimeStart;
    }

    public String getTimeEnd() {
        return mTimeEnd;
    }

    public void setTimeEnd(String mTimeEnd) {
        this.mTimeEnd = mTimeEnd;
    }

    public String getUserFbid() {
        return mUserFbid;
    }

    public void setUserFbid(String mUserFbid) {
        this.mUserFbid = mUserFbid;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String mUserName) {
        this.mUserName = mUserName;
    }

    public int getUserSid() {
        return mUserSid;
    }

    public void setUserSid(int mUserSid) {
        this.mUserSid = mUserSid;
    }
}