package jp.co.primesecurity.tapdoorcloud.mvp.view.activities;

import android.content.Context;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import jp.co.primesecurity.tapdoorcloud.R;
import jp.co.primesecurity.tapdoorcloud.mvp.base.BaseActivity;
import jp.co.primesecurity.tapdoorcloud.mvp.base.BaseFragment;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.Repository;
import jp.co.primesecurity.tapdoorcloud.mvp.presenter.MainPresenter;
import jp.co.primesecurity.tapdoorcloud.mvp.view.fragments.PageFragment;
import jp.co.primesecurity.tapdoorcloud.mvp.view.mvpview.MainMvpView;


public class MainActivity extends BaseActivity implements MainMvpView {

    private MainPresenter mPresenter;
    protected BaseFragment mBaseFragment;

    @Override
    protected int getContentViewResId() {
        return R.layout.activity_main;
    }

    @Override
    protected void init() {
        setContentMainId(R.id.fl_content);
        //Set up mPresenter
        mPresenter = new MainPresenter();
        mPresenter.attachView(this);
        //Set up ToolBar
        setSupportActionBar(mToolbar);
        mBaseFragment = new PageFragment();
        addFragment(mBaseFragment, mBaseFragment.getTag());

    }

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public Context getMyContext() {
        return this;
    }

    @Override
    public void showProgressIndicator() {

    }

    @Override
    public void showRepositories(List<Repository> list) {

    }

    @Override
    public void showMessage(String s) {

    }
}