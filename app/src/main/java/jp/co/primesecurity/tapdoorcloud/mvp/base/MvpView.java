package jp.co.primesecurity.tapdoorcloud.mvp.base;

import android.content.Context;

public interface MvpView {
    Context getMyContext();
}