package jp.co.primesecurity.tapdoorcloud.util;

import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.EditText;

import com.blankj.utilcode.util.LogUtils;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by thanh.vn on 8/4/2015.
 */
public class ValidationUtils {

    private static final String FOLDER_NAME_REGEX = "[<>:*?/\"\\|¥]";
    private static final String USERNAME_REGEX = "^[a-zA-Z0-9._]{2,50}$";
    private static final String METADATA_REGEX = "^[a-zA-Z0-9._/-]*$";
    private static final String NUMERIC_REGEX = "^[ 0-9._/-]*$";
    private static final String ALPHABET_REGEX = "^[ a-zA-Z._/-]*$";
    private static final String ALPHANUMERIC_REGEX = "^[ a-zA-Z0-9._/-]*$";
    private static final String ONE_DRIVE_FOLDER_NAME_REGEX = "[<>:*?/\"\\\\|]";
    private static final String ONLY_NUMERIC_REGEX = "^[0-9]+$";


    /**
     * Email validation pattern.
     */
    public static final Pattern EMAIL_PATTERN = android.util.Patterns.EMAIL_ADDRESS;

    /**
     * Email validation pattern follow DM.
     */
    public static final Pattern EMAIL_PATTERN_DM = Pattern.compile(
            "^(([a-zA-Z0-9\\-]+(?:\\.[a-zA-Z0-9\\-]+)*))+@(([a-zA-Z0-9]([a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])?\\.)+[a-zA-Z]{2,6})$"
    );

    /**
     * Check if the folder name contains special characters or not
     *
     * @param folderName
     * @return
     */
    public static boolean hasContainSpecialCharacters(final String folderName) {
        Pattern pattern = Pattern.compile(FOLDER_NAME_REGEX);
        Matcher matcher = pattern.matcher(folderName);
        return matcher.find();
    }

    /**
     * Check if the folder name contains special characters or not
     *
     * @param folderName
     * @return
     */
    public static boolean hasSpecialCharactersOneDriveFolderName(final String folderName) {
        Pattern pattern = Pattern.compile(ONE_DRIVE_FOLDER_NAME_REGEX);
        Matcher matcher = pattern.matcher(folderName);
        return matcher.find();
    }

    /**
     * Validate username with regular expression
     *
     * @param userName username for validation
     * @return true valid username, false invalid username
     */
    public static boolean validateUserName(final String userName) {
        Pattern pattern = Pattern.compile(USERNAME_REGEX);
        Matcher matcher = pattern.matcher(userName);
        return matcher.matches();
    }

    /**
     * Validate username with regular expression
     *
     * @param input username for validation
     * @return true valid username, false invalid username
     */
    public static boolean validateMetadata(final String input) {
        Pattern pattern = Pattern.compile(METADATA_REGEX);
        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }

    /**
     * Validate number with regular expression
     *
     * @param input number for validation
     * @return true valid number, false invalid number
     */
    public static boolean validateNumber(final String input) {
        Pattern pattern = Pattern.compile(NUMERIC_REGEX);
        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }

    /**
     * Validate alphabet with regular expression
     *
     * @param input string for validation
     * @return true valid alphabet, false invalid alphabet
     */
    public static boolean validateAlphabetical(final String input) {
        Pattern pattern = Pattern.compile(ALPHABET_REGEX);
        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }

    /**
     * Validate alphanumeric with regular expression
     *
     * @param input alphanumeric for validation
     * @return true valid alphanumeric, false alphanumeric alphabet
     */
    public static boolean validateAlphanumeric(final String input) {
        Pattern pattern = Pattern.compile(ALPHANUMERIC_REGEX);
        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }

    /**
     * Check if the string contains Emoji or not
     *
     * @param chkString
     * @return
     */
    public static boolean hasUsedEmoji(String chkString) {
        boolean ret = false;

        char[] ach = chkString.toCharArray();
        int cp;
        int len = chkString.length();

        for (int i = 0; i < len; i += Character.charCount(cp)) {
            char c = ach[i];
            cp = Character.codePointAt(ach, i);
            // 囲み数字
            if (0x20E3 == cp) {
                ret = true;
                break;
            }
            // 固定文字コード範囲を除外する
            else if ((0x2194 <= cp && cp <= 0x21F9) || // 矢印
                    (0x2300 <= cp && cp <= 0x23FF) || // その他の技術用記号
                    (0x249C <= cp && cp <= 0x24E9) || // 囲みアルファベット
                    (0x2500 <= cp && cp <= 0x27FF) || // 罫線素片、ブロック要素、その他の記号、装飾記号、その他の数学記号A、補助矢印A
                    (0x2900 <= cp && cp <= 0x297F) || // 補助矢印B
                    (0x2B00 <= cp && cp <= 0x2BFF) || // その他の記号及び矢印
                    (0x3290 <= cp && cp <= 0x32B0) || // 囲みCJK文字・月
                    (0xE000 <= cp && cp <= 0xF8FF) // 私用領域（外字領域）
                    ) {
                ret = true;
                break;
            } else if (Character.isHighSurrogate(c)) {
                // 上位サロゲート
                ret = true;
                break;
            } else {
                // その他、固定文字の除外判定
                if (cp == 0x00A9 || // ©
                        cp == 0x00AE || // ®
                        cp == 0x2122 || // ™
                        cp == 0x2139 || // ℹ
                        cp == 0x3030 || // 〰
                        cp == 0x303D // 〽
                        ) {
                    ret = true;
                    break;
                } else {
                    // 通常文字
                }
            }
        }

        return ret;
    }

    /**
     * Check if inputted string contains Japanese or not
     *
     * @param str : Input string
     * @return
     */
    public static boolean isContainJapanese(String str) {
        Set<Character.UnicodeBlock> japaneseUnicodeBlocks = new HashSet<Character.UnicodeBlock>() {{
            add(Character.UnicodeBlock.HIRAGANA);
            add(Character.UnicodeBlock.KATAKANA);
            add(Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS);
        }};
        for (char c : str.toCharArray()) {
            if (japaneseUnicodeBlocks.contains(Character.UnicodeBlock.of(c))) {
                System.out.println(c + " is a Japanese character");
                return true;
            }
        }
        return false;
    }

    /**
     * Method for validating of the email address syntax.
     *
     * @param emailIDs - string of multiple or single mail IDs
     * @return true if all mail IDs are correct else false
     */
    public static boolean isEmailsValid(String emailIDs) {
        if (emailIDs == null) {
            return false;
        }
        boolean valid = false;
        String[] emailSet = emailIDs.replaceAll(", +", ",").replaceAll(",+", ",").split(",");
        for (String anEmailSet : emailSet) {                                                                    // Validating Syntax of each mail ID
            CharSequence inputStr = anEmailSet.trim();
            if (isAnEmailValid(inputStr)) {
                valid = true;
            } else {
                valid = false;
                break;
            }
        }
        return valid;
    }

    /**
     * Method for validating of the email address syntax.
     *
     * @param emailIAddress - string of single mail address
     * @return true if all mail address are correct else false
     */
    public static boolean isAnEmailValid(CharSequence emailIAddress) {
        if (emailIAddress == null) {
            return false;
        }
        return EMAIL_PATTERN.matcher(emailIAddress).matches();
    }

    public static boolean isOnlyNumeric(CharSequence phoneNumber) {
        if (phoneNumber == null) {
            return false;
        }
        Pattern pattern = Pattern.compile(ONLY_NUMERIC_REGEX);
        Matcher matcher = pattern.matcher(phoneNumber);
        return matcher.matches();
    }

    public static void setNoSpaceEdittext(EditText edittext) {
        edittext.setFilters(new InputFilter[]{new NoInitialSpaceFilter()});
    }

    public static void setOnlyLetterDigitEdittext(final EditText edittext) {
        edittext.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new OnlyLetterDigitFilter()});
    }

    private static class NoInitialSpaceFilter implements InputFilter {
        @Override
        public CharSequence filter(final CharSequence source, final int start, final int end, final Spanned dest, final int dstart, final int dend) {
            boolean keepOriginal = true;
            StringBuilder sb = new StringBuilder(end - start);
            for (int i = start; i < end; i++) {
                char c = source.charAt(i);
                if (!Character.isSpaceChar(c)) {
                    // put your condition here
                    sb.append(c);
                } else {
                    LogUtils.i("Space:" , String.valueOf(c));
                    keepOriginal = false;
                }
            }
            if (keepOriginal)
                return null;
            else {
                if (source instanceof Spanned) {
                    SpannableString sp = new SpannableString(sb);
                    TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                    return sp;
                } else {
                    return sb;
                }
            }
        }
    }

    private static class OnlyLetterDigitFilter implements InputFilter {
        @Override
        public CharSequence filter(final CharSequence source, final int start, final int end, final Spanned dest, final int dstart, final int dend) {
            boolean keepOriginal = true;
            StringBuilder sb = new StringBuilder(end - start);
            for (int i = start; i < end; i++) {
                char c = source.charAt(i);
                if (Character.isLetterOrDigit(c)) {
                    // put your condition here
                    sb.append(c);
                } else
                    keepOriginal = false;
            }
            if (keepOriginal)
                return null;
            else {
                if (source instanceof Spanned) {
                    SpannableString sp = new SpannableString(sb);
                    TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                    return sp;
                } else {
                    return sb;
                }
            }
        }
    }
}
