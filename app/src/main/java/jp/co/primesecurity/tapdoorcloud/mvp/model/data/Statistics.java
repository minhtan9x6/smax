package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Statistics {

    @SerializedName("comment")
    @Expose
    private Integer comment;
    @SerializedName("subcomment")
    @Expose
    private Integer subcomment;
    @SerializedName("inbox")
    @Expose
    private Integer inbox;
    @SerializedName("message")
    @Expose
    private Integer message;
    @SerializedName("isnew")
    @Expose
    private Integer isnew;
    @SerializedName("post")
    @Expose
    private Integer post;
    @SerializedName("photo")
    @Expose
    private Integer photo;
    @SerializedName("album")
    @Expose
    private Integer album;

    public Integer getComment() {
        return comment;
    }

    public void setComment(Integer comment) {
        this.comment = comment;
    }

    public Integer getSubcomment() {
        return subcomment;
    }

    public void setSubcomment(Integer subcomment) {
        this.subcomment = subcomment;
    }

    public Integer getInbox() {
        return inbox;
    }

    public void setInbox(Integer inbox) {
        this.inbox = inbox;
    }

    public Integer getMessage() {
        return message;
    }

    public void setMessage(Integer message) {
        this.message = message;
    }

    public Integer getIsnew() {
        return isnew;
    }

    public void setIsnew(Integer isnew) {
        this.isnew = isnew;
    }

    public Integer getPost() {
        return post;
    }

    public void setPost(Integer post) {
        this.post = post;
    }

    public Integer getPhoto() {
        return photo;
    }

    public void setPhoto(Integer photo) {
        this.photo = photo;
    }

    public Integer getAlbum() {
        return album;
    }

    public void setAlbum(Integer album) {
        this.album = album;
    }

}