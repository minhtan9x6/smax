package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

public class Image {
    @SerializedName("width")
    private int mWidth;
    @SerializedName("src")
    private String mSrc;
    @SerializedName("height")
    private int mHeight;

    public int getWidth() {
        return mWidth;
    }

    public void setWidth(int mWidth) {
        this.mWidth = mWidth;
    }

    public String getSrc() {
        return mSrc;
    }

    public void setSrc(String mSrc) {
        this.mSrc = mSrc;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setHeight(int mHeight) {
        this.mHeight = mHeight;
    }
}