package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import jp.co.primesecurity.tapdoorcloud.mvp.model.data.ImageData;
import com.google.gson.annotations.SerializedName;

public class FbAttachmentInbox {
    @SerializedName("image_data")
    private ImageData mImageData;
    @SerializedName("size")
    private int mSize;
    @SerializedName("name")
    private String mName;
    @SerializedName("mime_type")
    private String mMimeType;
    @SerializedName("id")
    private String mId;

    public ImageData getImageData() {
        return mImageData;
    }

    public void setImageData(ImageData mImageData) {
        this.mImageData = mImageData;
    }

    public int getSize() {
        return mSize;
    }

    public void setSize(int mSize) {
        this.mSize = mSize;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getMimeType() {
        return mMimeType;
    }

    public void setMimeType(String mMimeType) {
        this.mMimeType = mMimeType;
    }

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }
}