package jp.co.primesecurity.tapdoorcloud.common;

public interface AppConst {
    String PREFERENCE_NAME = "SIN setting";
    String OK_HTTP_USER_AGENT = "app.com.shareitnow";
    String DEVELOPMENT = "DEVELOPMENT";
    String PRODUCTION = "PRODUCTION";
    String WEB_URL_PRODUCTION = "http://shareitnowapp.com";
    String WEB_URL_DEVELOPMENT = "http://shareitnowapp.com";
    String API_URL_PRODUCTION = "aHR0cDovL2FwaS5zaGFyZWl0bm93YXBwLmNvbS8=";


    String FORMAT_DATE_BIRTHDAY = "yyyy-MM-dd'T'HH:mm:ssZ";
    String FORMAT_DATE_REGISTRATION = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    String FORMAT_DATE_BIRTHDAY_VN = "dd/MM/yyyy";
    String FORMAT_DATE_BIRTHDAY_FACEBOOK = "MM/dd/yyyy";
    String FORMAT_DATE_BIRTHDAY_JP = "yyyy/MM/dd";
    String FORMAT_DATE_SMART_NEW = "yyyy-MM-dd HH:mm";
    String FORMAT_DATE_SMART_NEW_VIEW = "yyyy.MM.dd";
    String FORMAT_IMAGE_TIME_STAMP = "yyyyMMddHHmmssSSSS";

    String FOLDER_TEMP = "Temp";
    String APPEND_PATH_COMPONENT = "/";
    String APPEND_DOT_COMPONENT = ".";
    String FB_ID = "Fb ID";
    String CONFIG = "Config";
    String TAGS = "Tags";
    String THREADS = "Threads";
    String TYPE_PARENT = "Type parent";
    String PHONE_REGEX = "$phone";
    String POSITION = "position";
}
