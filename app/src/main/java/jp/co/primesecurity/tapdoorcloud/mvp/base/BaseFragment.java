package jp.co.primesecurity.tapdoorcloud.mvp.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.Toast;

import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.LogUtils;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import jp.co.primesecurity.tapdoorcloud.R;
import jp.co.primesecurity.tapdoorcloud.common.AppConst;
import jp.co.primesecurity.tapdoorcloud.mvp.model.prefs.AppSharedPreferences;

public abstract class BaseFragment extends Fragment implements MvpView {
    private final String TAG = this.getClass().getSimpleName();

    /**
     * Slide animation with right side in and left side out. And reversing screen with left side in and right side out.
     */
    public static final int ANIMATION_RIGHT_IN_LEFT_OUT = 1;
    /**
     * Slide animation with bottom side in and top side out. And reversing screen with top side in and bottom side out.
     */
    public static final int ANIMATION_BOTTOM_IN_TOP_OUT = 2;
    /**
     * no use animation.
     */
    public static final int ANIMATION_ONLY_APPLY_REVERSE = 3;

    /**
     * Slide animation with scalex side in and scalex side out. And reversing screen with left side in and right side out.
     */
    public static final int ANIMATION_SCALE_IN_RIGHT_OUT = 4;

    /**
     * No animation
     */
    public static final int NO_ANIMATION = 5;

    /**
     * Flag to set no header view to add
     */
    public static final int FLAG_NO_RESOURCE = 0;

    private boolean mIsAnimationEnd = true;

    private boolean isFragmentAlreadyLoaded;

    private boolean isAddKeyBackPress = true;

    private Fragment mPreviousFragment;

    private Activity mActivity;

    private Unbinder mUnbinder;

    public void setNeedReloadFragment(boolean isNeed) {
        this.isFragmentAlreadyLoaded = !isNeed;
    }

    public void setPreviousFragment(Fragment previousFragment) {
        this.mPreviousFragment = previousFragment;
    }

    public Fragment getPreviousFragment() {
        return this.mPreviousFragment;
    }

    private int counterFragmentCreated;

    private boolean isChildFragment;

    public boolean isChildFragment() {
        return isChildFragment;
    }

    public void setChildFragment(boolean childFragment) {
        isChildFragment = childFragment;
    }

    public void setCounterFragmentCreated(int counterFragmentCreated) {
        this.counterFragmentCreated = counterFragmentCreated;
    }

    public int getCounterFragmentCreated() {
        return this.counterFragmentCreated;
    }

    public boolean isRestoredFromBackStack() {
        return this.isFragmentAlreadyLoaded && this.counterFragmentCreated > 1;
    }

    /**
     * Queue data
     */
    private LinkedHashMap<String, LinkedList<Runnable>> mArrRunnableQue =
            new LinkedHashMap<String, LinkedList<Runnable>>();

    protected abstract int getContentResId();

    protected abstract void init();

    // root view attach other view to it
    protected View mViewContainer;

    public final View safeInflater(LayoutInflater pInflater, ViewGroup pContainer, int pLayoutResId, boolean pAttachToRoot) {
        View view = pInflater.inflate(pLayoutResId, pContainer, pAttachToRoot);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        this.counterFragmentCreated++;
        if (savedInstanceState == null && !this.isFragmentAlreadyLoaded) {
            this.isFragmentAlreadyLoaded = true;
            // Code placed here will be executed once
        }
        //Code placed here will be executed even when the fragment comes from backstack
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        LogUtils.i("onCreateView: " + TAG);
        try {
            mActivity = getActivity();
            isAddKeyBackPress = true;
            if (!isFragmentAlreadyLoaded) {
                mViewContainer = safeInflater(inflater, container, getContentResId(), false);
                KeyboardUtils.hideSoftInput(mActivity);
                init();
            }
        } catch (Exception e) {
            e.printStackTrace();
//            LogUtils.e(TAG + e.toString());
        }
        return mViewContainer;
    }

    protected void showToast(String message) {
        Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();
    }

    protected ActionBar getSupportActionBar() {
        return ((BaseActivity) getActivity()).getSupportActionBar();
    }

    protected void setSupportActionBar(Toolbar toolbar) {
        ((BaseActivity) getActivity()).setSupportActionBar(toolbar);
    }

    protected Activity getBaseActivity() {
        return getActivity();
    }

    private final BaseActivity.KeyBackPressCallBack mKeyBackPressCallBack = () -> {
        if (isFragmentValid()) {
            onKeyBackPress();
        }
    };

    protected final boolean isFragmentValid() {
        return isVisible() && isAdded() && isResumed() && !isRemoving() && !isDetached();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = getActivity();
        if (mActivity instanceof BaseActivity) {
            if (getFragmentManager().findFragmentById(((BaseActivity) mActivity).getContentMainId()) == this) {
                ((BaseActivity) context).addKeyBackCallBack(mKeyBackPressCallBack);
            }
        } else {
            LogUtils.e(TAG, "Activity not instanceof BaseActivity");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mUnbinder.unbind();
        if (mActivity instanceof BaseActivity) {
            if (getFragmentManager().findFragmentById(((BaseActivity) mActivity).getContentMainId()) == this) {
                ((BaseActivity) mActivity).removeKeyBackCallBack(mKeyBackPressCallBack);
            }
        }
    }

    public void showDialogFragment(BaseDialogFragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        fragment.show(fragmentManager, fragment.getClass().getName());
    }

    public void showDialogFragment(BaseDialogFragment fragment, Bundle bundle) {
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        FragmentManager fragmentManager = getFragmentManager();
        fragment.show(fragmentManager, fragment.getClass().getName());
    }

    public void showDialogFragmentFullScreen(BaseDialogFragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        fragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.Dialog_FullScreen);
        fragment.show(fragmentManager, fragment.getClass().getName());
    }

    public void showDialogFragmentFullScreen(BaseDialogFragment fragment, Bundle bundle) {
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        FragmentManager fragmentManager = getFragmentManager();
        fragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.Dialog_FullScreen);
        fragment.show(fragmentManager, fragment.getClass().getName());
    }


    public Fragment getPreviosFragment() {
        int index = getActivity().getSupportFragmentManager().getBackStackEntryCount() - 2;
        FragmentManager.BackStackEntry backEntry = getActivity().getSupportFragmentManager().getBackStackEntryAt(index);
        String tag = backEntry.getName();
        return getActivity().getSupportFragmentManager().findFragmentByTag(tag);
    }

    public void finishFragment() {
        if (mActivity instanceof BaseActivity) {
            int backStackCount = getFragmentManager().getBackStackEntryCount();
            if (backStackCount > 0) {
                LogUtils.d(TAG, "popping back stack");
                getFragmentManager().popBackStack();
            } else {
                LogUtils.d(TAG, "nothing on back stack, calling super");
                finishActivity();
            }
        }
    }

    protected void onKeyBackPress() {
        finishFragment();
    }

    public void finishActivity() {
        if (getActivity() != null) {
            getActivity().finish();
        }
    }

    public boolean isFragmentAnimationEnd() {
        return mIsAnimationEnd;
    }

    public Window getWindow() {
        return getActivity().getWindow();
    }

    public Object getSystemService(final String name) {
        return getActivity().getSystemService(name);
    }

    public final SharedPreferences getAppSharedPreferences() {
        return getActivity().getSharedPreferences(AppConst.PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    public final AppSharedPreferences getEtcSharedPreference() {
        return AppSharedPreferences.getInstance(getAppSharedPreferences());
    }

    /**
     * Replace fragment
     */
    public void replaceFragment(Class<? extends BaseFragment> pFragment) {
        this.replaceFragment(((BaseActivity)mActivity).getContentMainId(), pFragment, null, NO_ANIMATION);
    }

    /**
     * Replace fragment
     */
    public void replaceFragment(int pLayoutResId, Class<? extends BaseFragment> pFragment) {
        this.replaceFragment(pLayoutResId, pFragment, null, NO_ANIMATION);
    }

    /**
     * Replace fragment
     */
    public void replaceFragment(Class<? extends BaseFragment> pFragment, Bundle pBundle) {
        this.replaceFragment(((BaseActivity)mActivity).getContentMainId(), pFragment, pBundle, NO_ANIMATION);
    }

    /**
     * Replace fragment
     */
    public void replaceFragment(int pLayoutResId, Class<? extends BaseFragment> pFragment, Bundle pBundle) {
        this.replaceFragment(pLayoutResId, pFragment, pBundle, NO_ANIMATION);
    }

    /**
     * Replace fragment
     */
    public void replaceFragmentNoAnimation(int pLayoutResId, Class<? extends BaseFragment> pFragment, Bundle pBundle) {
        this.replaceFragment(pLayoutResId, pFragment, pBundle, NO_ANIMATION);
    }

    /**
     * Replace fragment with custom animation
     */
    public void replaceFragment(int pLayoutResId, Class<? extends BaseFragment> pFragment, Bundle pBundle, int pAnimation) {
        this.replaceFragment(pLayoutResId, pFragment, pFragment.getName(), pBundle, getFragmentManager(), pAnimation);
    }

    /**
     * Replace fragment with custom animation
     */
    public void replaceFragment(int pLayoutResId, Class<? extends BaseFragment> pFragment, FragmentManager fragmentManager) {
        this.replaceFragment(pLayoutResId, pFragment, pFragment.getName(), null, fragmentManager, NO_ANIMATION);
    }

    /**
     * Replace fragment with custom animation
     */
    public void replaceFragment(int pLayoutResId, Class<? extends BaseFragment> pFragment, Bundle pBundle, FragmentManager fragmentManager) {
        this.replaceFragment(pLayoutResId, pFragment, pFragment.getName(), pBundle, fragmentManager, NO_ANIMATION);
    }

    /**
     * Replace fragment with custom animation
     */
    public void replaceFragment(int pLayoutResId, Class<? extends BaseFragment> pFragment, Bundle pBundle, int pAnimation, boolean isStateLossAccepted) {
        this.replaceFragment(pLayoutResId, pFragment, pFragment.getName(), pBundle, getFragmentManager(), pAnimation, isStateLossAccepted);
    }

    public void clearBackStack(FragmentManager pFragmentManager) {
        pFragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public void clearBackStackAndKeepFirstFragment(FragmentManager pFragmentManager) {
        //http://stackoverflow.com/questions/5802141/is-this-the-right-way-to-clean-up-fragment-back-stack-when-leaving-a-deeply-nest
        if (pFragmentManager.getBackStackEntryCount() > 0) {
            int firstFragmentId = pFragmentManager.getBackStackEntryAt(0).getId();
            pFragmentManager.popBackStackImmediate(firstFragmentId, 0);
        }
    }

    /**
     * Replace fragment with custom animation
     *
     * @param pLayoutResId
     * @param pFragment
     * @param pBundle
     * @param pFragmentManager
     * @param pAnimation
     */
    public void replaceFragment(int pLayoutResId, Class<? extends BaseFragment> pFragment, String pTag, Bundle pBundle, FragmentManager pFragmentManager, int pAnimation) {
        try {
            final BaseFragment frag = pFragment.newInstance();
            this.replaceFragment(pLayoutResId, frag, pTag, pBundle, pFragmentManager, pAnimation);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    /**
     * Replace fragment with custom animation
     *
     * @param pLayoutResId
     * @param pFragment
     * @param pBundle
     * @param pFragmentManager
     * @param pAnimation
     * @param isStateLossAccepted
     */
    public void replaceFragment(int pLayoutResId, Class<? extends BaseFragment> pFragment, String pTag, Bundle pBundle, FragmentManager pFragmentManager, int pAnimation, boolean isStateLossAccepted) {
        try {
            final BaseFragment frag = pFragment.newInstance();
            this.replaceFragment(pLayoutResId, frag, pTag, pBundle, pFragmentManager, pAnimation, isStateLossAccepted);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void replaceFragment(int pLayoutResId, BaseFragment pFragment, FragmentManager pFragmentManager) {
        try {
            this.replaceFragment(pLayoutResId, pFragment, pFragment.getTag(), null, pFragmentManager, NO_ANIMATION);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void replaceFragment(int pLayoutResId, BaseFragment pFragment, FragmentManager pFragmentManager, Bundle bundle) {
        try {
            this.replaceFragment(pLayoutResId, pFragment, pFragment.getTag(), bundle, pFragmentManager, NO_ANIMATION);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void replaceFragment(int pLayoutResId, BaseFragment pFragment, Bundle pBundle, FragmentManager pFragmentManager, int pAnimation) {
        try {
            this.replaceFragment(pLayoutResId, pFragment, pFragment.getTag(), pBundle, pFragmentManager, pAnimation);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void replaceFragment(int pLayoutResId, BaseFragment pFragment, int pAnimation) {
        this.replaceFragment(pLayoutResId, pFragment, null, null, getFragmentManager(), pAnimation);
    }

    private void replaceFragment(int pLayoutResId, BaseFragment pFragment, String pTag, Bundle pBundle, FragmentManager pFragmentManager, int pAnimation) {
        replaceFragment(pLayoutResId, pFragment, pTag, pBundle, pFragmentManager, pAnimation, false);
    }

    private void replaceFragment(int pLayoutResId, BaseFragment pFragment, String pTag, Bundle pBundle, FragmentManager pFragmentManager, int pAnimation, boolean isStateLossAccepted) {
        KeyboardUtils.hideSoftInput(mActivity);
        final FragmentTransaction fragmentTransaction = pFragmentManager.beginTransaction();
        switch (pAnimation) {
            case ANIMATION_BOTTOM_IN_TOP_OUT:
//                    fragmentTransaction.setCustomAnimations(R.animator.animator_bottom_in, R.animator.animator_top_out, R.animator.animator_top_in, R.animator.animator_bottom_out);
                fragmentTransaction.setCustomAnimations(R.anim.anim_bottom_in, R.anim.anim_top_out, R.anim.anim_top_in, R.anim.anim_bottom_out);
                break;
            case ANIMATION_RIGHT_IN_LEFT_OUT:
//                    fragmentTransaction.setCustomAnimations(R.animator.animator_right_in, R.animator.animator_left_out, R.animator.animator_left_in, R.animator.animator_right_out);
                fragmentTransaction.setCustomAnimations(R.anim.anim_right_in, R.anim.anim_left_out, R.anim.anim_left_in, R.anim.anim_right_out);
                break;
            case ANIMATION_SCALE_IN_RIGHT_OUT:
//                    fragmentTransaction.setCustomAnimations(R.animator.animator_scalex_enter, R.animator.animator_scalex_exit, R.animator.animator_left_in, R.animator.animator_right_out);
                fragmentTransaction.setCustomAnimations(R.anim.anim_scalex_enter, R.anim.anim_scalex_exit, R.anim.anim_left_in, R.anim.anim_right_out);
                break;
            case ANIMATION_ONLY_APPLY_REVERSE:
//                    fragmentTransaction.setCustomAnimations(R.animator.animator_none, R.animator.animator_none, R.animator.animator_left_in, R.animator.animator_right_out);
                fragmentTransaction.setCustomAnimations(R.anim.anim_none, R.anim.anim_none, R.anim.anim_left_in, R.anim.anim_right_out);
                break;
            default: // No animation
                break;
        }
        if (pBundle != null) {
            pFragment.setArguments(pBundle);
        }
        pFragment.setPreviousFragment(this);
        final String backStateName = pTag != null ? pTag : pFragment.getClass().getName();
        fragmentTransaction.addToBackStack(backStateName);
        fragmentTransaction.replace(pLayoutResId, pFragment, backStateName);
        if (isStateLossAccepted)
            fragmentTransaction.commitAllowingStateLoss();
        else
            fragmentTransaction.commit();
    }

    public void replaceFragmentNotAddBackStack(int pLayoutResId, BaseFragment pFragment) {
        replaceFragmentNotAddBackStack(pLayoutResId, pFragment, null);
    }

    public void replaceFragmentNotAddBackStack(int pLayoutResId, BaseFragment pFragment, Bundle pBundle) {
        replaceFragmentNotAddBackStack(pLayoutResId, pFragment, pBundle, getFragmentManager());
    }

    public void replaceFragmentNotAddBackStack(int pLayoutResId, BaseFragment pFragment, Bundle pBundle, FragmentManager pFragmentManager) {
        replaceFragmentNotAddBackStack(pLayoutResId, pFragment, pBundle, pFragmentManager, ANIMATION_RIGHT_IN_LEFT_OUT);
    }

    public void replaceFragmentNotAddBackStack(int pLayoutResId, BaseFragment pFragment, Bundle pBundle, int pAnimation) {
        this.replaceFragmentNotAddBackStack(pLayoutResId, pFragment, pBundle, getFragmentManager(), pAnimation);
    }

    public void replaceFragmentNotAddBackStack(int pLayoutResId, BaseFragment pFragment, Bundle pBundle, FragmentManager pFragmentManager, int pAnimation) {
        KeyboardUtils.hideSoftInput(mActivity);
        final FragmentTransaction fragmentTransaction = pFragmentManager.beginTransaction();
        switch (pAnimation) {
            case ANIMATION_BOTTOM_IN_TOP_OUT:
                fragmentTransaction.setCustomAnimations(R.anim.anim_bottom_in, R.anim.anim_top_out, R.anim.anim_top_in, R.anim.anim_bottom_out);
                break;
            case ANIMATION_RIGHT_IN_LEFT_OUT:
                fragmentTransaction.setCustomAnimations(R.anim.anim_right_in, R.anim.anim_left_out, R.anim.anim_left_in, R.anim.anim_right_out);
                break;
            case ANIMATION_SCALE_IN_RIGHT_OUT:
                fragmentTransaction.setCustomAnimations(R.anim.anim_scalex_enter, R.anim.anim_scalex_exit, R.anim.anim_left_in, R.anim.anim_right_out);
                break;
            case ANIMATION_ONLY_APPLY_REVERSE:
                fragmentTransaction.setCustomAnimations(R.anim.anim_none, R.anim.anim_none, R.anim.anim_left_in, R.anim.anim_right_out);
                break;
            default: // No animation
                break;
        }
        if (pBundle != null) {
            pFragment.setArguments(pBundle);
        }

        fragmentTransaction.replace(pLayoutResId, pFragment, null);
        fragmentTransaction.commit();
    }

    /**
     * Handle on fragment animation started
     */
    protected void onAnimationStarted() {
        runQueue();
    }

    /**
     * Handle on fragment animation ended
     */
    protected void onAnimationEnded() {
        runQueue();
    }

    private void initIfNotExitKey(final String pKey) {
        if (mArrRunnableQue.get(pKey) == null) {
            Log.e("Queue", pKey + " => no queue found in key init it");
            mArrRunnableQue.put(pKey, new LinkedList<Runnable>());
        }
    }

    public void clearQueueByKey(final String pKey) {
        initIfNotExitKey(pKey);
        mArrRunnableQue.get(pKey).clear();
    }

    private void runQueue() {
        for (Map.Entry<String, LinkedList<Runnable>> entry : mArrRunnableQue.entrySet()) {
            // System.out.println(entry.getKey() + "/" + entry.getValue());
            for (Runnable pRunnable : entry.getValue()) {
                if (pRunnable != null) {
                    pRunnable.run();
                }
            }
        }
        mArrRunnableQue.clear();
    }

    /**
     * Handle on fragment animation repeated
     */
    protected void onAnimationRepeated() {
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation animation = super.onCreateAnimation(transit, enter, nextAnim);
        return animation;
    }

    public final Animation.AnimationListener animationListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
            LogUtils.d("onAnimationStarted");
            onAnimationStarted();
            mIsAnimationEnd = false;
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            LogUtils.d("onAnimationEnd");
            onAnimationEnded();
            mIsAnimationEnd = true;
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
            onAnimationRepeated();
        }
    };

    protected void hideProgressDialog(ProgressDialog dialog) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    protected void showProgressDialog(ProgressDialog dialog) {
        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
    }

    public boolean isAddKeyBackPress() {
        return isAddKeyBackPress;
    }

    protected boolean ignoreAddKeyBackPress() {
        return false;
    }


}
