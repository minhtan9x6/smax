package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Pagination {
    @SerializedName("prelink")
    private String mPrelink;
    @SerializedName("current")
    private int mCurrent;
    @SerializedName("previous")
    private String mPrevious;
    @SerializedName("next")
    private String mNext;
    @SerializedName("first")
    private String mFirst;
    @SerializedName("last")
    private String mLast;
    @SerializedName("range")
    private List<String> mRange;
    @SerializedName("fromResult")
    private int mFromresult;
    @SerializedName("toResult")
    private int mToresult;
    @SerializedName("totalResult")
    private int mTotalresult;
    @SerializedName("pageCount")
    private int mPagecount;

    public String getPrelink() {
        return mPrelink;
    }

    public void setPrelink(String mPrelink) {
        this.mPrelink = mPrelink;
    }

    public int getCurrent() {
        return mCurrent;
    }

    public void setCurrent(int mCurrent) {
        this.mCurrent = mCurrent;
    }

    public String getPrevious() {
        return mPrevious;
    }

    public void setPrevious(String mPrevious) {
        this.mPrevious = mPrevious;
    }

    public String getNext() {
        return mNext;
    }

    public void setNext(String mNext) {
        this.mNext = mNext;
    }

    public String getFirst() {
        return mFirst;
    }

    public void setFirst(String mFirst) {
        this.mFirst = mFirst;
    }

    public String getLast() {
        return mLast;
    }

    public void setLast(String mLast) {
        this.mLast = mLast;
    }

    public List<String> getRange() {
        return mRange;
    }

    public void setRange(List<String> mRange) {
        this.mRange = mRange;
    }

    public int getFromresult() {
        return mFromresult;
    }

    public void setFromresult(int mFromresult) {
        this.mFromresult = mFromresult;
    }

    public int getToresult() {
        return mToresult;
    }

    public void setToresult(int mToresult) {
        this.mToresult = mToresult;
    }

    public int getTotalresult() {
        return mTotalresult;
    }

    public void setTotalresult(int mTotalresult) {
        this.mTotalresult = mTotalresult;
    }

    public int getPagecount() {
        return mPagecount;
    }

    public void setPagecount(int mPagecount) {
        this.mPagecount = mPagecount;
    }
}