package jp.co.primesecurity.tapdoorcloud.mvp.base;

public interface Presenter<V extends MvpView> {

    void attachView(V view);

    void detachView();

}