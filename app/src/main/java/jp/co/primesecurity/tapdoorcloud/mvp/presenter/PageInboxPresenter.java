package jp.co.primesecurity.tapdoorcloud.mvp.presenter;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import jp.co.primesecurity.tapdoorcloud.R;
import jp.co.primesecurity.tapdoorcloud.mvp.base.BasePresenter;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.DetailFbPageResponse;
import jp.co.primesecurity.tapdoorcloud.mvp.model.data.Filter;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.InboxResponse;
import jp.co.primesecurity.tapdoorcloud.mvp.view.mvpview.PageInboxMvpView;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by thanhvn on 12/18/17.
 */

public class PageInboxPresenter extends BasePresenter<PageInboxMvpView> {

    private CompositeDisposable mCompositeDisposable;

    @Override
    public void attachView(PageInboxMvpView view) {
        super.attachView(view);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }
    }

    public void loadPageInfo(boolean isLoading, String fbId, Filter filter) {
//        GetBiPageInboxTask getBiPageInboxTask = new GetBiPageInboxTask(getContext(), isLoading, fbId, new ThreadRequest(filter));
//        mCompositeDisposable.add(getBiPageInboxTask.setOnCombineCallbackListener(new BaseBiCallbackTask.OnCombineCallbackListener() {
//            @Override
//            public void combineResult(Object result1, Object result2) {
//                DetailFbPageResponse detailFbPageResponse = (DetailFbPageResponse) result1;
//                InboxResponse inboxResponse = (InboxResponse) result2;
//                if (detailFbPageResponse != null && inboxResponse != null) {
//                    getMvpView().showInfoPage(inboxResponse.getData().getThreads(), detailFbPageResponse.getData().getTags());
//                }
//            }
//        }));
        Gson gson = new Gson();
        DetailFbPageResponse detailFbPageResponse = gson.fromJson(convertJson(R.raw.response1),DetailFbPageResponse.class);
        InboxResponse inboxResponse = gson.fromJson(convertJson(R.raw.response2),InboxResponse.class);
        getMvpView().showInfoPage(inboxResponse.getData().getThreads(), detailFbPageResponse.getData().getTags());
    }

    public void loadThreads(String fbId, Filter filter){
//        PostInboxThreadTask getFbPageTask = new PostInboxThreadTask(getContext(), fbId, new ThreadRequest(filter));
//        mCompositeDisposable.add(getFbPageTask.observable(new BaseCallback.OnFinishCallbackListener() {
//            @Override
//            public void onSuccess(Object pResult, Headers headers) {
//                InboxResponse inboxResponse = (InboxResponse) pResult;
//                if (inboxResponse != null) {
//                    if (inboxResponse.getData() != null) {
//                        getMvpView().showThreads(inboxResponse.getData().getThreads());
//                    }
//                }
//            }
//
//            @Override
//            public void onError(Object error, Headers headers) {
//
//            }
//
//            @Override
//            public void onFailed(Integer pResult) {
//
//            }
//        }));
        Gson gson = new Gson();
        InboxResponse inboxResponse = gson.fromJson(convertJson(R.raw.response2),InboxResponse.class);
        getMvpView().showThreads(inboxResponse.getData().getThreads());
    }

    private String convertJson(int resId) {

        InputStream inputStream = getContext().getResources().openRawResource(resId);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String json = null;
        try {
            json = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

//    public void listenEvent(Activity activity) {
//        if (activity instanceof MainActivity) {
//            MainActivity mainActivity = (MainActivity) activity;
//            RxBus rxBus = mainActivity.getRxBusSingleton();
//            mCompositeDisposable.add(rxBus.asFlowable().subscribe(event -> {
//                if (event instanceof UpdateThreadNewEvent) {
//                    UpdateThreadNewEvent threadNewEvent = (UpdateThreadNewEvent) event;
//                    getMvpView().updateThreadNew(threadNewEvent.getThreads());
//                }
//            }));
//        }
//    }
}
