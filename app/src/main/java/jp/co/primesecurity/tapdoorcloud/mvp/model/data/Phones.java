package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Phones implements Parcelable {
    @SerializedName("_id")
    private String mId;
    @SerializedName("number")
    private String mNumber;
    @SerializedName("fbname")
    private String mFbname;
    @SerializedName("fbid")
    private String mFbid;

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String mNumber) {
        this.mNumber = mNumber;
    }

    public String getFbname() {
        return mFbname;
    }

    public void setFbname(String mFbname) {
        this.mFbname = mFbname;
    }

    public String getFbid() {
        return mFbid;
    }

    public void setFbid(String mFbid) {
        this.mFbid = mFbid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mNumber);
        dest.writeString(this.mFbname);
        dest.writeString(this.mFbid);
    }

    protected Phones(Parcel in) {
        this.mId = in.readString();
        this.mNumber = in.readString();
        this.mFbname = in.readString();
        this.mFbid = in.readString();
    }

    public static final Creator<Phones> CREATOR = new Creator<Phones>() {
        @Override
        public Phones createFromParcel(Parcel source) {
            return new Phones(source);
        }

        @Override
        public Phones[] newArray(int size) {
            return new Phones[size];
        }
    };
}