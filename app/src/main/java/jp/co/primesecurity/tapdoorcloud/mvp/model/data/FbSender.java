package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class FbSender implements Parcelable {
    @SerializedName("fbid")
    private String mFbid;
    @SerializedName("fbname")
    private String mFbname;

    public String getFbid() {
        return mFbid;
    }

    public void setFbid(String mFbid) {
        this.mFbid = mFbid;
    }

    public String getFbname() {
        return mFbname;
    }

    public void setFbname(String mFbname) {
        this.mFbname = mFbname;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mFbid);
        dest.writeString(this.mFbname);
    }

    public FbSender() {
    }

    protected FbSender(Parcel in) {
        this.mFbid = in.readString();
        this.mFbname = in.readString();
    }

    public static final Creator<FbSender> CREATOR = new Creator<FbSender>() {
        @Override
        public FbSender createFromParcel(Parcel source) {
            return new FbSender(source);
        }

        @Override
        public FbSender[] newArray(int size) {
            return new FbSender[size];
        }
    };
}