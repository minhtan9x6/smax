package jp.co.primesecurity.tapdoorcloud.mvp.model.network.event;

import jp.co.primesecurity.tapdoorcloud.mvp.model.data.Tags;

/**
 * Created by thanhvn on 1/1/18.
 */

public class EditTagEvent {
    private boolean mIsEdit;
    private Tags mTags;

    public EditTagEvent(boolean isEdit, Tags tags) {
        mIsEdit = isEdit;
        mTags = tags;
    }

    public boolean isEdit() {
        return mIsEdit;
    }

    public void setEdit(boolean edit) {
        mIsEdit = edit;
    }

    public Tags getTags() {
        return mTags;
    }

    public void setTags(Tags tags) {
        mTags = tags;
    }
}
