package jp.co.primesecurity.tapdoorcloud.util.widget.linkableText;

/**
 * Created by 4qualia on 2/8/2017.
 * Copyright © 2016年 tcl. All rights reserved.
 */

public class Range {
    public int start;
    public int end;

    public Range(int start, int end) {
        this.start = start;
        this.end = end;
    }
}
