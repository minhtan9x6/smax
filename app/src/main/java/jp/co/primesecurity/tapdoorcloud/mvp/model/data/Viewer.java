package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

/**
 * Created by tan.lm on 17/01/2018.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Viewer implements Parcelable {

    @SerializedName("time")
    @Expose
    private long time;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("sid")
    @Expose
    private Integer sid;

    protected Viewer(Parcel in) {
        if (in.readByte() == 0) {
            time = 0;
        } else {
            time = in.readInt();
        }
        name = in.readString();
        if (in.readByte() == 0) {
            sid = null;
        } else {
            sid = in.readInt();
        }
    }

    public static final Creator<Viewer> CREATOR = new Creator<Viewer>() {
        @Override
        public Viewer createFromParcel(Parcel in) {
            return new Viewer(in);
        }

        @Override
        public Viewer[] newArray(int size) {
            return new Viewer[size];
        }
    };

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (time == 0) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(time);
        }
        dest.writeString(name);
        if (sid == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(sid);
        }
    }
}
