package jp.co.primesecurity.tapdoorcloud.mvp.view.mvpview;



import java.util.List;

import jp.co.primesecurity.tapdoorcloud.mvp.base.MvpView;
import jp.co.primesecurity.tapdoorcloud.mvp.model.network.response.Repository;


public interface MainMvpView extends MvpView {
    void showProgressIndicator();
    void showRepositories(List<Repository> list);
    void showMessage(String s);

}