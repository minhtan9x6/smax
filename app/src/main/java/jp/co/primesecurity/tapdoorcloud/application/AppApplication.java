package jp.co.primesecurity.tapdoorcloud.application;

import android.content.Context;
import android.content.SharedPreferences;

import android.support.multidex.MultiDexApplication;

import com.blankj.utilcode.util.Utils;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import jp.co.primesecurity.tapdoorcloud.common.AppConst;
import jp.co.primesecurity.tapdoorcloud.mvp.model.prefs.AppSharedPreferences;


/**
 * Created by thanh.vn on 1/16/2017.
 */

public class AppApplication extends MultiDexApplication {

    private Scheduler mDefaultSubscribeScheduler;

    @Override
    public void onCreate() {
        Utils.init(this);
        super.onCreate();
    }

    public static AppApplication get(Context context) {
        return (AppApplication) context.getApplicationContext();
    }

    public SharedPreferences getSharedPreferences() {
        return getApplicationContext().getSharedPreferences(AppConst.PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    public AppSharedPreferences getAppSharedPreference() {
        return AppSharedPreferences.getInstance(getSharedPreferences());
    }

    public Scheduler defaultSubscribeScheduler() {
        if (mDefaultSubscribeScheduler == null) {
            mDefaultSubscribeScheduler = Schedulers.io();
        }
        return mDefaultSubscribeScheduler;
    }

    //User to change scheduler from tests
    public void setDefaultSubscribeScheduler(Scheduler scheduler) {
        this.mDefaultSubscribeScheduler = scheduler;
    }
}
