package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Viewing implements Parcelable {
    @SerializedName("sid")
    private int mSid;
    @SerializedName("name")
    private String mName;
    @SerializedName("picture")
    private String mPicture;
    @SerializedName("time")
    private String mTime;
    @SerializedName("socketid")
    private String mSocketid;

    public int getSid() {
        return mSid;
    }

    public void setSid(int mSid) {
        this.mSid = mSid;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getPicture() {
        return mPicture;
    }

    public void setPicture(String mPicture) {
        this.mPicture = mPicture;
    }

    public String getTime() {
        return mTime;
    }

    public void setTime(String mTime) {
        this.mTime = mTime;
    }

    public String getSocketid() {
        return mSocketid;
    }

    public void setSocketid(String mSocketid) {
        this.mSocketid = mSocketid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mSid);
        dest.writeString(this.mName);
        dest.writeString(this.mPicture);
        dest.writeString(this.mTime);
        dest.writeString(this.mSocketid);
    }

    public Viewing() {
    }

    protected Viewing(Parcel in) {
        this.mSid = in.readInt();
        this.mName = in.readString();
        this.mPicture = in.readString();
        this.mTime = in.readString();
        this.mSocketid = in.readString();
    }

    public static final Creator<Viewing> CREATOR = new Creator<Viewing>() {
        @Override
        public Viewing createFromParcel(Parcel source) {
            return new Viewing(source);
        }

        @Override
        public Viewing[] newArray(int size) {
            return new Viewing[size];
        }
    };
}