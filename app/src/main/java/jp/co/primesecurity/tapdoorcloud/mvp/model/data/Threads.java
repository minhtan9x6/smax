package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Threads implements Parcelable {
    @SerializedName("_id")
    private String mId;
    @SerializedName("fbupdated_time")
    private String mFbupdatedTime;
    @SerializedName("count_msg")
    private String mCountMsg;
    @SerializedName("type_parent")
    private String mTypeParent;
    @SerializedName("type")
    private String mType;
    @SerializedName("fblink")
    private String mFblink;
    @SerializedName("fbpageid")
    private String mFbpageid;
    @SerializedName("fbmsg")
    private String mFbmsg;
    @SerializedName("fbid")
    private String mFbid;
    @SerializedName("__v")
    private int mV;
    @SerializedName("count_replied")
    private int mCountReplied;
    @SerializedName("isnew")
    private boolean mIsnew;
    @SerializedName("replied")
    private boolean mReplied;
    @SerializedName("orders")
    private List<Orders> mOrders;
    @SerializedName("viewing")
    private Viewing mViewing;
    @SerializedName("viewer")
    private Viewer mViewer;
    @SerializedName("phones")
    private List<Phones> mPhones;
    @SerializedName("customers")
    private List<Customers> mCustomers;
    @SerializedName("tags")
    private List<String> mTags;
    @SerializedName("fbsender")
    private FbSender mFbSender;

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getFbupdatedTime() {
        return mFbupdatedTime;
    }

    public void setFbupdatedTime(String mFbupdatedTime) {
        this.mFbupdatedTime = mFbupdatedTime;
    }

    public String getCountMsg() {
        return mCountMsg;
    }

    public void setCountMsg(String mCountMsg) {
        this.mCountMsg = mCountMsg;
    }

    public String getTypeParent() {
        return mTypeParent;
    }

    public void setTypeParent(String mTypeParent) {
        this.mTypeParent = mTypeParent;
    }

    public String getType() {
        return mType;
    }

    public void setType(String mType) {
        this.mType = mType;
    }

    public String getFblink() {
        return mFblink;
    }

    public void setFblink(String mFblink) {
        this.mFblink = mFblink;
    }

    public String getFbpageid() {
        return mFbpageid;
    }

    public void setFbpageid(String mFbpageid) {
        this.mFbpageid = mFbpageid;
    }

    public String getFbmsg() {
        return mFbmsg;
    }

    public void setFbmsg(String mFbmsg) {
        this.mFbmsg = mFbmsg;
    }

    public String getFbid() {
        return mFbid;
    }

    public void setFbid(String mFbid) {
        this.mFbid = mFbid;
    }

    public int getV() {
        return mV;
    }

    public void setV(int mV) {
        this.mV = mV;
    }

    public int getCountReplied() {
        return mCountReplied;
    }

    public void setCountReplied(int mCountReplied) {
        this.mCountReplied = mCountReplied;
    }

    public boolean getIsnew() {
        return mIsnew;
    }

    public void setIsnew(boolean mIsnew) {
        this.mIsnew = mIsnew;
    }

    public boolean getReplied() {
        return mReplied;
    }

    public void setReplied(boolean mReplied) {
        this.mReplied = mReplied;
    }

    public List<Orders> getOrders() {
        return mOrders;
    }

    public void setOrders(List<Orders> mOrders) {
        this.mOrders = mOrders;
    }

    public Viewing getViewing() {
        return mViewing;
    }

    public void setViewing(Viewing mViewing) {
        this.mViewing = mViewing;
    }

    public Viewer getViewer() {
        return mViewer;
    }

    public void setViewer(Viewer mViewer) {
        this.mViewer = mViewer;
    }

    public List<Phones> getPhones() {
        return mPhones;
    }

    public void setPhones(List<Phones> mPhones) {
        this.mPhones = mPhones;
    }

    public List<Customers> getCustomers() {
        return mCustomers;
    }

    public void setCustomers(List<Customers> mCustomers) {
        this.mCustomers = mCustomers;
    }

    public List<String> getTags() {
        return mTags;
    }

    public void setTags(List<String> mTags) {
        this.mTags = mTags;
    }

    public FbSender getFbSender() {
        return mFbSender;
    }

    public void setFbSender(FbSender mFbSender) {
        this.mFbSender = mFbSender;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mId);
        dest.writeString(this.mFbupdatedTime);
        dest.writeString(this.mCountMsg);
        dest.writeString(this.mTypeParent);
        dest.writeString(this.mType);
        dest.writeString(this.mFblink);
        dest.writeString(this.mFbpageid);
        dest.writeString(this.mFbmsg);
        dest.writeString(this.mFbid);
        dest.writeInt(this.mV);
        dest.writeInt(this.mCountReplied);
        dest.writeByte(this.mIsnew ? (byte) 1 : (byte) 0);
        dest.writeByte(this.mReplied ? (byte) 1 : (byte) 0);
        dest.writeList(this.mOrders);
        dest.writeParcelable(this.mViewing, flags);
        dest.writeParcelable(this.mViewer, flags);
        dest.writeTypedList(this.mPhones);
        dest.writeTypedList(this.mCustomers);
        dest.writeStringList(this.mTags);
        dest.writeParcelable(this.mFbSender, flags);
    }

    public Threads() {
    }

    protected Threads(Parcel in) {
        this.mId = in.readString();
        this.mFbupdatedTime = in.readString();
        this.mCountMsg = in.readString();
        this.mTypeParent = in.readString();
        this.mType = in.readString();
        this.mFblink = in.readString();
        this.mFbpageid = in.readString();
        this.mFbmsg = in.readString();
        this.mFbid = in.readString();
        this.mV = in.readInt();
        this.mCountReplied = in.readInt();
        this.mIsnew = in.readByte() != 0;
        this.mReplied = in.readByte() != 0;
        this.mOrders = new ArrayList<Orders>();
        in.readList(this.mOrders, Orders.class.getClassLoader());
        this.mViewing = in.readParcelable(Viewing.class.getClassLoader());
        this.mViewer = in.readParcelable(Viewer.class.getClassLoader());
        this.mPhones = in.createTypedArrayList(Phones.CREATOR);
        this.mCustomers = in.createTypedArrayList(Customers.CREATOR);
        this.mTags = in.createStringArrayList();
        this.mFbSender = in.readParcelable(FbSender.class.getClassLoader());
    }

    public static final Creator<Threads> CREATOR = new Creator<Threads>() {
        @Override
        public Threads createFromParcel(Parcel source) {
            return new Threads(source);
        }

        @Override
        public Threads[] newArray(int size) {
            return new Threads[size];
        }
    };
}