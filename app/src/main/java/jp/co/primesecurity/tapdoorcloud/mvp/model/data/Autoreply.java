package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Autoreply {
    @SerializedName("scripts")
    private List<Scripts> mScripts;
    @SerializedName("phone")
    private Phone mPhone;

    public List<Scripts> getScripts() {
        return mScripts;
    }

    public void setScripts(List<Scripts> mScripts) {
        this.mScripts = mScripts;
    }

    public Phone getPhone() {
        return mPhone;
    }

    public void setPhone(Phone mPhone) {
        this.mPhone = mPhone;
    }
}