package jp.co.primesecurity.tapdoorcloud.mvp.model.data;

import com.google.gson.annotations.SerializedName;

public class DataSubAttachment {
    @SerializedName("url")
    private String mUrl;
    @SerializedName("type")
    private String mType;
    @SerializedName("target")
    private Target mTarget;
    @SerializedName("media")
    private Media mMedia;

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public String getType() {
        return mType;
    }

    public void setType(String mType) {
        this.mType = mType;
    }

    public Target getTarget() {
        return mTarget;
    }

    public void setTarget(Target mTarget) {
        this.mTarget = mTarget;
    }

    public Media getMedia() {
        return mMedia;
    }

    public void setMedia(Media mMedia) {
        this.mMedia = mMedia;
    }
}